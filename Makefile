# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.5

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/maciek/Desktop/sat_solvery/sat-solver-cpp

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/maciek/Desktop/sat_solvery/sat-solver-cpp

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "No interactive CMake dialog available..."
	/usr/bin/cmake -E echo No\ interactive\ CMake\ dialog\ available.
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/maciek/Desktop/sat_solvery/sat-solver-cpp/CMakeFiles /home/maciek/Desktop/sat_solvery/sat-solver-cpp/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/maciek/Desktop/sat_solvery/sat-solver-cpp/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named solver

# Build rule for target.
solver: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 solver
.PHONY : solver

# fast build rule for target.
solver/fast:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/build
.PHONY : solver/fast

src/SatSolver.Core/Solver.o: src/SatSolver.Core/Solver.cpp.o

.PHONY : src/SatSolver.Core/Solver.o

# target to build an object file
src/SatSolver.Core/Solver.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Core/Solver.cpp.o
.PHONY : src/SatSolver.Core/Solver.cpp.o

src/SatSolver.Core/Solver.i: src/SatSolver.Core/Solver.cpp.i

.PHONY : src/SatSolver.Core/Solver.i

# target to preprocess a source file
src/SatSolver.Core/Solver.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Core/Solver.cpp.i
.PHONY : src/SatSolver.Core/Solver.cpp.i

src/SatSolver.Core/Solver.s: src/SatSolver.Core/Solver.cpp.s

.PHONY : src/SatSolver.Core/Solver.s

# target to generate assembly for a file
src/SatSolver.Core/Solver.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Core/Solver.cpp.s
.PHONY : src/SatSolver.Core/Solver.cpp.s

src/SatSolver.Drup/DRUPProofHelper.o: src/SatSolver.Drup/DRUPProofHelper.cpp.o

.PHONY : src/SatSolver.Drup/DRUPProofHelper.o

# target to build an object file
src/SatSolver.Drup/DRUPProofHelper.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Drup/DRUPProofHelper.cpp.o
.PHONY : src/SatSolver.Drup/DRUPProofHelper.cpp.o

src/SatSolver.Drup/DRUPProofHelper.i: src/SatSolver.Drup/DRUPProofHelper.cpp.i

.PHONY : src/SatSolver.Drup/DRUPProofHelper.i

# target to preprocess a source file
src/SatSolver.Drup/DRUPProofHelper.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Drup/DRUPProofHelper.cpp.i
.PHONY : src/SatSolver.Drup/DRUPProofHelper.cpp.i

src/SatSolver.Drup/DRUPProofHelper.s: src/SatSolver.Drup/DRUPProofHelper.cpp.s

.PHONY : src/SatSolver.Drup/DRUPProofHelper.s

# target to generate assembly for a file
src/SatSolver.Drup/DRUPProofHelper.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Drup/DRUPProofHelper.cpp.s
.PHONY : src/SatSolver.Drup/DRUPProofHelper.cpp.s

src/SatSolver.Parser/ParseResult.o: src/SatSolver.Parser/ParseResult.cpp.o

.PHONY : src/SatSolver.Parser/ParseResult.o

# target to build an object file
src/SatSolver.Parser/ParseResult.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Parser/ParseResult.cpp.o
.PHONY : src/SatSolver.Parser/ParseResult.cpp.o

src/SatSolver.Parser/ParseResult.i: src/SatSolver.Parser/ParseResult.cpp.i

.PHONY : src/SatSolver.Parser/ParseResult.i

# target to preprocess a source file
src/SatSolver.Parser/ParseResult.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Parser/ParseResult.cpp.i
.PHONY : src/SatSolver.Parser/ParseResult.cpp.i

src/SatSolver.Parser/ParseResult.s: src/SatSolver.Parser/ParseResult.cpp.s

.PHONY : src/SatSolver.Parser/ParseResult.s

# target to generate assembly for a file
src/SatSolver.Parser/ParseResult.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Parser/ParseResult.cpp.s
.PHONY : src/SatSolver.Parser/ParseResult.cpp.s

src/SatSolver.Parser/Parser.o: src/SatSolver.Parser/Parser.cpp.o

.PHONY : src/SatSolver.Parser/Parser.o

# target to build an object file
src/SatSolver.Parser/Parser.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Parser/Parser.cpp.o
.PHONY : src/SatSolver.Parser/Parser.cpp.o

src/SatSolver.Parser/Parser.i: src/SatSolver.Parser/Parser.cpp.i

.PHONY : src/SatSolver.Parser/Parser.i

# target to preprocess a source file
src/SatSolver.Parser/Parser.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Parser/Parser.cpp.i
.PHONY : src/SatSolver.Parser/Parser.cpp.i

src/SatSolver.Parser/Parser.s: src/SatSolver.Parser/Parser.cpp.s

.PHONY : src/SatSolver.Parser/Parser.s

# target to generate assembly for a file
src/SatSolver.Parser/Parser.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Parser/Parser.cpp.s
.PHONY : src/SatSolver.Parser/Parser.cpp.s

src/SatSolver.Types/BoundedArrayQueue.o: src/SatSolver.Types/BoundedArrayQueue.cpp.o

.PHONY : src/SatSolver.Types/BoundedArrayQueue.o

# target to build an object file
src/SatSolver.Types/BoundedArrayQueue.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/BoundedArrayQueue.cpp.o
.PHONY : src/SatSolver.Types/BoundedArrayQueue.cpp.o

src/SatSolver.Types/BoundedArrayQueue.i: src/SatSolver.Types/BoundedArrayQueue.cpp.i

.PHONY : src/SatSolver.Types/BoundedArrayQueue.i

# target to preprocess a source file
src/SatSolver.Types/BoundedArrayQueue.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/BoundedArrayQueue.cpp.i
.PHONY : src/SatSolver.Types/BoundedArrayQueue.cpp.i

src/SatSolver.Types/BoundedArrayQueue.s: src/SatSolver.Types/BoundedArrayQueue.cpp.s

.PHONY : src/SatSolver.Types/BoundedArrayQueue.s

# target to generate assembly for a file
src/SatSolver.Types/BoundedArrayQueue.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/BoundedArrayQueue.cpp.s
.PHONY : src/SatSolver.Types/BoundedArrayQueue.cpp.s

src/SatSolver.Types/Clause.o: src/SatSolver.Types/Clause.cpp.o

.PHONY : src/SatSolver.Types/Clause.o

# target to build an object file
src/SatSolver.Types/Clause.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Clause.cpp.o
.PHONY : src/SatSolver.Types/Clause.cpp.o

src/SatSolver.Types/Clause.i: src/SatSolver.Types/Clause.cpp.i

.PHONY : src/SatSolver.Types/Clause.i

# target to preprocess a source file
src/SatSolver.Types/Clause.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Clause.cpp.i
.PHONY : src/SatSolver.Types/Clause.cpp.i

src/SatSolver.Types/Clause.s: src/SatSolver.Types/Clause.cpp.s

.PHONY : src/SatSolver.Types/Clause.s

# target to generate assembly for a file
src/SatSolver.Types/Clause.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Clause.cpp.s
.PHONY : src/SatSolver.Types/Clause.cpp.s

src/SatSolver.Types/Literal.o: src/SatSolver.Types/Literal.cpp.o

.PHONY : src/SatSolver.Types/Literal.o

# target to build an object file
src/SatSolver.Types/Literal.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Literal.cpp.o
.PHONY : src/SatSolver.Types/Literal.cpp.o

src/SatSolver.Types/Literal.i: src/SatSolver.Types/Literal.cpp.i

.PHONY : src/SatSolver.Types/Literal.i

# target to preprocess a source file
src/SatSolver.Types/Literal.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Literal.cpp.i
.PHONY : src/SatSolver.Types/Literal.cpp.i

src/SatSolver.Types/Literal.s: src/SatSolver.Types/Literal.cpp.s

.PHONY : src/SatSolver.Types/Literal.s

# target to generate assembly for a file
src/SatSolver.Types/Literal.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Literal.cpp.s
.PHONY : src/SatSolver.Types/Literal.cpp.s

src/SatSolver.Types/PropagationQueue.o: src/SatSolver.Types/PropagationQueue.cpp.o

.PHONY : src/SatSolver.Types/PropagationQueue.o

# target to build an object file
src/SatSolver.Types/PropagationQueue.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/PropagationQueue.cpp.o
.PHONY : src/SatSolver.Types/PropagationQueue.cpp.o

src/SatSolver.Types/PropagationQueue.i: src/SatSolver.Types/PropagationQueue.cpp.i

.PHONY : src/SatSolver.Types/PropagationQueue.i

# target to preprocess a source file
src/SatSolver.Types/PropagationQueue.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/PropagationQueue.cpp.i
.PHONY : src/SatSolver.Types/PropagationQueue.cpp.i

src/SatSolver.Types/PropagationQueue.s: src/SatSolver.Types/PropagationQueue.cpp.s

.PHONY : src/SatSolver.Types/PropagationQueue.s

# target to generate assembly for a file
src/SatSolver.Types/PropagationQueue.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/PropagationQueue.cpp.s
.PHONY : src/SatSolver.Types/PropagationQueue.cpp.s

src/SatSolver.Types/Trail.o: src/SatSolver.Types/Trail.cpp.o

.PHONY : src/SatSolver.Types/Trail.o

# target to build an object file
src/SatSolver.Types/Trail.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Trail.cpp.o
.PHONY : src/SatSolver.Types/Trail.cpp.o

src/SatSolver.Types/Trail.i: src/SatSolver.Types/Trail.cpp.i

.PHONY : src/SatSolver.Types/Trail.i

# target to preprocess a source file
src/SatSolver.Types/Trail.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Trail.cpp.i
.PHONY : src/SatSolver.Types/Trail.cpp.i

src/SatSolver.Types/Trail.s: src/SatSolver.Types/Trail.cpp.s

.PHONY : src/SatSolver.Types/Trail.s

# target to generate assembly for a file
src/SatSolver.Types/Trail.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Trail.cpp.s
.PHONY : src/SatSolver.Types/Trail.cpp.s

src/SatSolver.Types/Variable.o: src/SatSolver.Types/Variable.cpp.o

.PHONY : src/SatSolver.Types/Variable.o

# target to build an object file
src/SatSolver.Types/Variable.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Variable.cpp.o
.PHONY : src/SatSolver.Types/Variable.cpp.o

src/SatSolver.Types/Variable.i: src/SatSolver.Types/Variable.cpp.i

.PHONY : src/SatSolver.Types/Variable.i

# target to preprocess a source file
src/SatSolver.Types/Variable.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Variable.cpp.i
.PHONY : src/SatSolver.Types/Variable.cpp.i

src/SatSolver.Types/Variable.s: src/SatSolver.Types/Variable.cpp.s

.PHONY : src/SatSolver.Types/Variable.s

# target to generate assembly for a file
src/SatSolver.Types/Variable.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/Variable.cpp.s
.PHONY : src/SatSolver.Types/Variable.cpp.s

src/SatSolver.Types/VariableManager.o: src/SatSolver.Types/VariableManager.cpp.o

.PHONY : src/SatSolver.Types/VariableManager.o

# target to build an object file
src/SatSolver.Types/VariableManager.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/VariableManager.cpp.o
.PHONY : src/SatSolver.Types/VariableManager.cpp.o

src/SatSolver.Types/VariableManager.i: src/SatSolver.Types/VariableManager.cpp.i

.PHONY : src/SatSolver.Types/VariableManager.i

# target to preprocess a source file
src/SatSolver.Types/VariableManager.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/VariableManager.cpp.i
.PHONY : src/SatSolver.Types/VariableManager.cpp.i

src/SatSolver.Types/VariableManager.s: src/SatSolver.Types/VariableManager.cpp.s

.PHONY : src/SatSolver.Types/VariableManager.s

# target to generate assembly for a file
src/SatSolver.Types/VariableManager.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/VariableManager.cpp.s
.PHONY : src/SatSolver.Types/VariableManager.cpp.s

src/SatSolver.Types/VariableOrderHeap.o: src/SatSolver.Types/VariableOrderHeap.cpp.o

.PHONY : src/SatSolver.Types/VariableOrderHeap.o

# target to build an object file
src/SatSolver.Types/VariableOrderHeap.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/VariableOrderHeap.cpp.o
.PHONY : src/SatSolver.Types/VariableOrderHeap.cpp.o

src/SatSolver.Types/VariableOrderHeap.i: src/SatSolver.Types/VariableOrderHeap.cpp.i

.PHONY : src/SatSolver.Types/VariableOrderHeap.i

# target to preprocess a source file
src/SatSolver.Types/VariableOrderHeap.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/VariableOrderHeap.cpp.i
.PHONY : src/SatSolver.Types/VariableOrderHeap.cpp.i

src/SatSolver.Types/VariableOrderHeap.s: src/SatSolver.Types/VariableOrderHeap.cpp.s

.PHONY : src/SatSolver.Types/VariableOrderHeap.s

# target to generate assembly for a file
src/SatSolver.Types/VariableOrderHeap.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/VariableOrderHeap.cpp.s
.PHONY : src/SatSolver.Types/VariableOrderHeap.cpp.s

src/SatSolver.Types/WatchedLiterals.o: src/SatSolver.Types/WatchedLiterals.cpp.o

.PHONY : src/SatSolver.Types/WatchedLiterals.o

# target to build an object file
src/SatSolver.Types/WatchedLiterals.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/WatchedLiterals.cpp.o
.PHONY : src/SatSolver.Types/WatchedLiterals.cpp.o

src/SatSolver.Types/WatchedLiterals.i: src/SatSolver.Types/WatchedLiterals.cpp.i

.PHONY : src/SatSolver.Types/WatchedLiterals.i

# target to preprocess a source file
src/SatSolver.Types/WatchedLiterals.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/WatchedLiterals.cpp.i
.PHONY : src/SatSolver.Types/WatchedLiterals.cpp.i

src/SatSolver.Types/WatchedLiterals.s: src/SatSolver.Types/WatchedLiterals.cpp.s

.PHONY : src/SatSolver.Types/WatchedLiterals.s

# target to generate assembly for a file
src/SatSolver.Types/WatchedLiterals.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/SatSolver.Types/WatchedLiterals.cpp.s
.PHONY : src/SatSolver.Types/WatchedLiterals.cpp.s

src/main.o: src/main.cpp.o

.PHONY : src/main.o

# target to build an object file
src/main.cpp.o:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/main.cpp.o
.PHONY : src/main.cpp.o

src/main.i: src/main.cpp.i

.PHONY : src/main.i

# target to preprocess a source file
src/main.cpp.i:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/main.cpp.i
.PHONY : src/main.cpp.i

src/main.s: src/main.cpp.s

.PHONY : src/main.s

# target to generate assembly for a file
src/main.cpp.s:
	$(MAKE) -f CMakeFiles/solver.dir/build.make CMakeFiles/solver.dir/src/main.cpp.s
.PHONY : src/main.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... rebuild_cache"
	@echo "... edit_cache"
	@echo "... solver"
	@echo "... src/SatSolver.Core/Solver.o"
	@echo "... src/SatSolver.Core/Solver.i"
	@echo "... src/SatSolver.Core/Solver.s"
	@echo "... src/SatSolver.Drup/DRUPProofHelper.o"
	@echo "... src/SatSolver.Drup/DRUPProofHelper.i"
	@echo "... src/SatSolver.Drup/DRUPProofHelper.s"
	@echo "... src/SatSolver.Parser/ParseResult.o"
	@echo "... src/SatSolver.Parser/ParseResult.i"
	@echo "... src/SatSolver.Parser/ParseResult.s"
	@echo "... src/SatSolver.Parser/Parser.o"
	@echo "... src/SatSolver.Parser/Parser.i"
	@echo "... src/SatSolver.Parser/Parser.s"
	@echo "... src/SatSolver.Types/BoundedArrayQueue.o"
	@echo "... src/SatSolver.Types/BoundedArrayQueue.i"
	@echo "... src/SatSolver.Types/BoundedArrayQueue.s"
	@echo "... src/SatSolver.Types/Clause.o"
	@echo "... src/SatSolver.Types/Clause.i"
	@echo "... src/SatSolver.Types/Clause.s"
	@echo "... src/SatSolver.Types/Literal.o"
	@echo "... src/SatSolver.Types/Literal.i"
	@echo "... src/SatSolver.Types/Literal.s"
	@echo "... src/SatSolver.Types/PropagationQueue.o"
	@echo "... src/SatSolver.Types/PropagationQueue.i"
	@echo "... src/SatSolver.Types/PropagationQueue.s"
	@echo "... src/SatSolver.Types/Trail.o"
	@echo "... src/SatSolver.Types/Trail.i"
	@echo "... src/SatSolver.Types/Trail.s"
	@echo "... src/SatSolver.Types/Variable.o"
	@echo "... src/SatSolver.Types/Variable.i"
	@echo "... src/SatSolver.Types/Variable.s"
	@echo "... src/SatSolver.Types/VariableManager.o"
	@echo "... src/SatSolver.Types/VariableManager.i"
	@echo "... src/SatSolver.Types/VariableManager.s"
	@echo "... src/SatSolver.Types/VariableOrderHeap.o"
	@echo "... src/SatSolver.Types/VariableOrderHeap.i"
	@echo "... src/SatSolver.Types/VariableOrderHeap.s"
	@echo "... src/SatSolver.Types/WatchedLiterals.o"
	@echo "... src/SatSolver.Types/WatchedLiterals.i"
	@echo "... src/SatSolver.Types/WatchedLiterals.s"
	@echo "... src/main.o"
	@echo "... src/main.i"
	@echo "... src/main.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

