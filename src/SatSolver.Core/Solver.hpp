#ifndef SOLVER_H
#define SOLVER_H

#include "SolverParams.hpp"
#include "../SatSolver.Types/SolverState.hpp"
#include "../SatSolver.Types/Literal.hpp"
#include "../SatSolver.Types/Clause.hpp"
#include "../SatSolver.Types/VariableManager.hpp"
#include "../SatSolver.Types/WatchedLiterals.hpp"
#include "../SatSolver.Types/PropagationQueue.hpp"
#include "../SatSolver.Types/BoundedArrayQueue.hpp"
#include "../SatSolver.Types/Trail.hpp"
#include "ReduceDatabaseComparers.hpp"
#include "../SatSolver.Drup/DRUPProofHelper.hpp"

#include <ctime>
#include <climits>

class Solver {
public:
	Solver(int variablesCount, int clausesCount);

	bool SolveSATInstance();

	void CheckModelCorrectness();

	void PrintResult();

	bool AddClause(vector<Literal>& literals, bool fromConflict, int clausewidth);

	SolverState GetState();

private:

	int _variablesCount;
	int _restartsCount;
	int _conflictsCount;
	int _databaseReductionsCount;
	int _learntClauseReductionsCount;
	double _learntClauseReductionsSum;
	unsigned int _currentConflictsCount;
	unsigned int _currentConflictsCount2;
	int _deletedClausesCount; // deleted in SimplifyClauseDatabase at decision level 0
	int _unitSimplifiedClausesCount;
	long long _propagationsCount;
	long long _learntClausesCount;
	double _agility;
	unsigned int _learntClausesLimit;
	unsigned int _conflictsLimitForAgilityRestart;
	unsigned int _decisions;
	
	unsigned int _conflictsLimitForTrivialRestart;
	unsigned int _conflictsLimitForLubyRestart;
	unsigned int _lubyConflictsLimitToAdjustLearntSize;
	unsigned int _lubyCurrentConflictsToAdjustLearntSize;
	double _trivialConflictsLimitToAdjustLearntSize;
	unsigned int _trivialCurrentConflictsToAdjustLearntSize;

	SolverParams _solverParams;
	SolverState _state;
	int _verbosity;
	bool _useSizeBasedClauseComparer;
	unsigned int _clauseid;
	unsigned int _successfulSubsumptionsCount;
	unsigned int _successfulStrenghtenClausesCount;
	unsigned int _pureliteralPropagations;
	unsigned int _halfClauseLBD;
	unsigned int _blockedRestarts;
	double _totalLBDSum;
	unsigned long long _blockersTrue;

	vector<SharedClauseReference> _clauses;
	vector<SharedClauseReference> _learntClauses;

	vector<LogicalValue> _model;
	PropagationQueue _propagationQueue;
	WatchedLiterals _watchedLiterals;
	VariableManager _variableManager;
	Trail _trail;
	DRUPProofHelper _drupProofHelper;
	BoundedArrayQueue _lbdQueue;
	BoundedArrayQueue _trailQueue;


	int clbd7;
	int clbd10;
	int clbd15;
	int clbd20;
	int clbd30;

	LogicalValue GetLiteralValue(const Literal& literal);

	//Literal GetLiteralToPropagate();

	void UncheckedPropagateLiteral(Literal literal, SharedClauseReference conflict);

	bool TryPropagateLiteral(Literal literal, SharedClauseReference conflict);

	void SetState(SolverState state);

	bool ChangeClauseWatchedLiteral(SharedClauseReference clause, Literal& baseLiteral, Literal& watchedLiteral, Literal& blocker);

	bool CheckTrivialClauseSatisfiability(vector<Literal>& literals);

	bool RemoveLiteralsDuplicates(vector<Literal>& literals);

	SharedClauseReference BooleanConstraintPropagation();

	void AssignNextVariable();

	void AssignNextVariableByOccurences(int variableNo);
	
	void AssignNextVariableByWatches(int variableNo);

	void AssignNextVariableByOccurencesAndWatches(int variableNo);

	void AssignNextVariableByRandom(int variableNo);

	void AssignNextVariableByPhaseSaving(int variableNo);
	
	void AssignNextVariableTrue(int variableNo);
	
	void AssignNextVariableFalse(int variableNo);

	void HandleConflict(SharedClauseReference conflict);

	pair<int, int> TraverseImplicationGraph(SharedClauseReference conflict, vector<Literal>& learntClause);

	//void CalculateReason(vector<Literal>& conflictLiterals, vector<Literal>& reason, Literal p);

	SharedClauseReference GetLastVisitedVariableReason(Literal& p, vector<bool>& visited);

	unsigned int ComputeLBD(SharedClauseReference clause);

	void CancelAssignments(int backtracklevel);

	bool ShouldDecayVariableActivities(int iterNo);

	bool ShouldDecayClauseActivities(int iterNo);

	void OnRestart();

	void UpdateAgility(Literal& literal);

	bool ShouldRestart();

	void BeforeRestart();
	
	void BlockRestart();

	double Luby(int restartsCount);

	void AdjustLubyLearntsLimit();

	void AdjustTrivialLearntsLimit();

	bool ShouldReduceClauseDatabase();

	void ReduceClauseDatabase();

	void SortLearntClauses();

	SolverState SimplifyClauseDatabase();

	bool IsClauseLocked(SharedClauseReference clause);

	void SimplifyLearntClause(vector<Literal>& learnt_clause, vector<bool>& visited);
	
	void SimplifyLearntClauseSimple(vector<Literal>& learnt_clause, vector<bool>& visited);
	
	void SimplifyLearntClauseRecursive(vector<Literal>& learnt_clause, vector<bool>& visited);

	bool IsLiteralDomiated(Literal& p, unsigned int abstract_levels, vector<Literal>& recursion_stack, vector<int>& clear_visited_variables_onerror, vector<bool>& visited);

	int CalculateBacktrackLevelForLearntClause(vector<Literal>& learnt_clause);

	void DecayClauseActivities();

	void BumpClauseActivity(SharedClauseReference cref);

	void TryFindAssignment();

	void ProcessSubsumptionForNewClause(SharedClauseReference cref);
	
	void StrenghtenClause(SharedClauseReference cref, int litIndex);
	
	void ProcessPureLiteralElimination();
	
	void RemoveClause(SharedClauseReference cref);

	void PrintStats();

	void SetClauseSortingStrategy();

	void CheckConfigurationErrors();
	
	void SetLubyUnitFactor();

	void RaiseError(string ErrorMessage);

};



#endif // SOLVER_H
