#ifndef SOLVERPARAMS_H
#define SOLVERPARAMS_H



struct SolverParams {
	double conflict_variable_activity_increase = 1.0;

	double conflict_clause_activity_increase = 1.0;

	double variable_activity_decay_factor = 0.95;

	double clause_activity_decay_factor = 0.999;

	double variable_activity_rescale_factor = 1e-100;

	double variable_activity_limit_rescale = 1e100;

	double clause_activity_rescale_factor = 1e-20;

	double clause_activity_limit_rescale = 1e20;

	unsigned int max_small_clause_size = 2;

	unsigned int max_lbd_for_frozen_clause = 5; // changed by solver during computation
	
	unsigned int lbd_for_frozen_clause_bound = 5; // cannot be more

	bool use_clause_activity_comparer = true;
	
	bool use_clause_width_comparer = false;

	unsigned int max_clause_size_for_size_based_comparer = 0; // turned off for now

	unsigned int initial_learnt_clauses_limit = 500;
	
	int clause_minimization_strategy = 2; // 0-none, 1-simple, 2-recursive
	
	//restart strategies
	bool use_trivial_restarts_strategy = false;
	
	bool use_agility_based_restarts_strategy = false;

	bool use_luby_restart_strategy = true;
	
	bool use_average_lbd_restart_strategy = false;
	
	bool require_unsat_proof = false;
	
	bool show_model = true;

	//agility based restart strategy parameters
	double restart_agility_limit = 0.3;

	double force_restart_agility_limit = 0.2;

	double magic_agility_decay_factor = 0.9999;

	unsigned int initial_conflicts_limit_for_agility_restart = 500;

	double learnt_clauses_limit_increase_factor_for_agility_restart = 1.25;

	double conflicts_limit_increase_factor_for_agility_restart = 1.5;

	//luby restart strategy parameters
	double luby_power_factor = 2.0;

	double luby_unit_factor = 100.0;

	unsigned int initial_luby_conflicts_to_adjust_learnt_size = 500;
	
	double luby_conflicts_increase_factor = 1.5;

	double learnt_clauses_limit_increase_factor_for_luby_restart = 1.25;
	
	unsigned int initial_trivial_conflicts_to_adjust_learnt_size = 100;
	
	double trivial_conflicts_increase_factor = 1.5;
	
	double learnt_clauses_limit_increase_factor_for_trivial_restart = 1.1;
	
	//average lbd restart strategies
	double K = 0.8;
	
	double R = 1.4;
	
	int lbd_queue_capacity = 50;
	
	int trail_queue_capacity = 5000;
	
	int lower_bound_for_blocking_restart = 10000;
	
	int start_reduceDB_conflicts_for_lbd_restarts = 4000;
	
	int reduceDB_conflicts_increment_for_lbd_restarts = 300;
	
	//Weighted VSIDS
	//bool enable_weighted_VSIDS_heuristic = false;
	
	bool use_weighted_VSIDS_heuristic = false; // do not change

	double weighted_VSIDS_G = 1.05;

	//clause subsumption
	bool enable_clause_subsumption = false;

	bool enable_clause_subsumption_for_instance_clauses = false;

	bool enable_clause_subsumption_for_learnt_clauses = false;

	bool check_instance_clauses_subsumption_for_learnt_clause = false;

	bool enable_clause_strenghten = false;
	
	//pure literal elimination
	bool enable_pure_literal_elimination = false;
	
};

#endif // SOLVERPARAMS_H
