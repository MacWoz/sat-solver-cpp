#ifndef REDUCEDATABASECOMPARERS_H
#define REDUCEDATABASECOMPARERS_H

#include "../SatSolver.Types/Clause.hpp"


struct ClauseActivityComparer {
	unsigned int max_small_clause_size;
	ClauseActivityComparer(unsigned int max_small_clause_size) {
		this->max_small_clause_size = max_small_clause_size;
	}

	bool operator () (SharedClauseReference c1, SharedClauseReference c2) {
		if (c1->LiteralsCount() <= max_small_clause_size)
			return false;

		if (c2->LiteralsCount() <= max_small_clause_size)
			return true;

		return c1->GetActivity() < c2->GetActivity();
	}
};

// po LBD malejaco, po activity rosnaco
struct ClauseLBDComparer {
	unsigned int max_small_clause_size;
	ClauseLBDComparer(unsigned int max_small_clause_size) {
		this->max_small_clause_size = max_small_clause_size;
	}

	bool operator () (SharedClauseReference c1, SharedClauseReference c2) {
		if (c1->LiteralsCount() <= max_small_clause_size)
			return false;
		if (c2->LiteralsCount() <= max_small_clause_size)
			return true;

		if (c1->GetLBD() > c2->GetLBD())
			return true;
		if (c1->GetLBD() < c2->GetLBD())
			return false;

		return c1->GetActivity() < c2->GetActivity();
	}
};

struct ClauseWidthComparer {
	unsigned int max_small_clause_size;
	ClauseWidthComparer(unsigned int max_small_clause_size) {
		this->max_small_clause_size = max_small_clause_size;
	}

	bool operator () (SharedClauseReference c1, SharedClauseReference c2) {
		if (c1->LiteralsCount() <= max_small_clause_size)
			return false;
		if (c2->LiteralsCount() <= max_small_clause_size)
			return true;
		if (c1->GetWidth() > c2->GetWidth())
			return true;
		if (c1->GetWidth() < c2->GetWidth())
			return false;
		
		return c1->GetActivity() < c2->GetActivity();
	}
};

struct ClauseSizeComparer {
	unsigned int max_small_clause_size;
	ClauseSizeComparer(unsigned int max_small_clause_size) {
		this->max_small_clause_size = max_small_clause_size;
	}

	bool operator () (SharedClauseReference c1, SharedClauseReference c2) {
		if (c1->LiteralsCount() <= max_small_clause_size)
			return false;
		if (c2->LiteralsCount() <= max_small_clause_size)
			return true;

		if (c1->LiteralsCount() > c2->LiteralsCount())
			return true;
		if (c1->LiteralsCount() < c2->LiteralsCount())
			return false;

		return c1->GetActivity() < c2->GetActivity();
	}
};

#endif // REDUCEDATABASECOMPARERS_H
