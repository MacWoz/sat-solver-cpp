
#include "Solver.hpp"


Solver::Solver(int variablesCount, int clausesCount) : _solverParams(), _model(variablesCount, LogicalValue::None), _watchedLiterals(variablesCount), _variableManager(variablesCount), _trail(variablesCount), _drupProofHelper(""), _lbdQueue(_solverParams.lbd_queue_capacity), _trailQueue(_solverParams.trail_queue_capacity)
{
	_state = SolverState::Unknown;
	_variablesCount = variablesCount;
	_restartsCount = 0;
	_blockedRestarts = 0;
	_conflictsCount = 0;
	_learntClausesCount = 0;
	_currentConflictsCount = 0;
	_currentConflictsCount2 = 0;
	_deletedClausesCount = 0;
	_unitSimplifiedClausesCount = 0;
	_databaseReductionsCount = 0;
	_learntClauseReductionsCount = 0;
	_learntClauseReductionsSum = 0.0;
	_successfulSubsumptionsCount = 0;
	_successfulStrenghtenClausesCount = 0;
	_propagationsCount = 0;
	_conflictsLimitForAgilityRestart = _solverParams.initial_conflicts_limit_for_agility_restart;
	_conflictsLimitForLubyRestart = 0;
	_conflictsLimitForTrivialRestart = 100;
	_lubyConflictsLimitToAdjustLearntSize = _solverParams.initial_luby_conflicts_to_adjust_learnt_size;
	_lubyCurrentConflictsToAdjustLearntSize = _lubyConflictsLimitToAdjustLearntSize;
	_trivialConflictsLimitToAdjustLearntSize = (double)_solverParams.initial_trivial_conflicts_to_adjust_learnt_size;
	_trivialCurrentConflictsToAdjustLearntSize = _trivialConflictsLimitToAdjustLearntSize;
	_learntClausesLimit = _solverParams.initial_learnt_clauses_limit;
	_halfClauseLBD = 0;
	_blockersTrue = 0;
	_decisions = 0;
	
	_verbosity = 1;
	_useSizeBasedClauseComparer = false;
	_agility = 1.0;
	_clauseid = 0;
	_pureliteralPropagations = 0;
	
	clbd7 = 0;
	clbd10 = 0;
	clbd15 = 0;
	clbd20 = 0;
	clbd30 = 0;
	
	_totalLBDSum = 0.0;


	if (_solverParams.require_unsat_proof)
		_drupProofHelper = DRUPProofHelper("./proof.out");
		
	if (_solverParams.use_trivial_restarts_strategy)
		_learntClausesLimit = (clausesCount / 3.0);
	
	if (_solverParams.use_average_lbd_restart_strategy)
		_learntClausesLimit = _solverParams.start_reduceDB_conflicts_for_lbd_restarts;
		
		
	
	
}

void Solver::SetState(SolverState state) {
	this->_state = state;
}

SolverState Solver::GetState() {
	return this->_state;
}

LogicalValue Solver::GetLiteralValue(const Literal& literal) {
	LogicalValue value = this->_model[literal.GetVariableNo()];
	if (value == LogicalValue::None)
		return value;
	if (literal.GetSign() == LiteralSign_Positive)
		return value;

	if (value == LogicalValue::True)
		return LogicalValue::False;
	else
		return LogicalValue::True;
}

bool Solver::SolveSATInstance() {
	srand(time(NULL));

	//CheckConfigurationErrors();
	//SetClauseSortingStrategy();
	//SetLubyUnitFactor();

	while (true) {
		this->TryFindAssignment();
		if (this->GetState() != SolverState::Restart)
			break;

		this->OnRestart();
	}


	if (this->GetState() == SolverState::Satisfiable)
		return true;
	if (this->GetState() == SolverState::Unsatisfiable)
		return false;

	RaiseError("Incorrect solver state in SolveSATInstance");
	return false;

}

//only for instance clauses, not for learnt
bool Solver::CheckTrivialClauseSatisfiability(vector<Literal>& literals) {
	int lastVariableNo = -1, currentVariableNo = 0;
	unsigned currentSign = LiteralSign_Unset, lastSign = LiteralSign_Unset;

	for (unsigned i = 0; i<literals.size(); i++) {
		LogicalValue literalValue = this->GetLiteralValue(literals[i]);
		if (literalValue == LogicalValue::True)
			return true;
		
		currentVariableNo = literals[i].GetVariableNo();
		currentSign = literals[i].GetSign();
		if (currentVariableNo == lastVariableNo) {
			if ((currentVariableNo == lastVariableNo) && (currentSign != lastSign))
				return true;

		}
		lastVariableNo = currentVariableNo;
		lastSign = currentSign;
	}
	return false;
}

bool Solver::RemoveLiteralsDuplicates(vector<Literal>& literals) {

	bool anythingRemoved = false;
	unsigned current_pos = 1, current_pos_distinct = 1;

	int lastVariableNo = literals[0].GetVariableNo(), currentVariableNo = literals[0].GetVariableNo();
	unsigned currentSign = literals[0].GetSign(), lastSign = literals[0].GetSign();
	
	while (current_pos < literals.size()) {
		LogicalValue literalValue = this->GetLiteralValue(literals[current_pos]);
		currentVariableNo = literals[current_pos].GetVariableNo();
		currentSign = literals[current_pos].GetSign();
		
		if ((currentVariableNo == lastVariableNo) && (currentSign == lastSign)) {
			current_pos++;
			anythingRemoved = true;
		}
		else if (literalValue == LogicalValue::False) {
			current_pos++;
			anythingRemoved = true;
		}
		else {
			literals[current_pos_distinct] = literals[current_pos];
			current_pos++;
			current_pos_distinct++;
			lastVariableNo = currentVariableNo;
			lastSign = currentSign;
		}
	}
	literals.resize(current_pos_distinct);
	
	return anythingRemoved;
}

bool Solver::AddClause(vector<Literal>& literals, bool fromConflict, int clausewidth) {
	if (literals.size() == 0)
		return true;

	bool replaceExistingClause = false; // only for certified UNSAT
	vector<Literal> oldLiterals = literals;

	bool isSat = false;

	if (!fromConflict) {
		sort(literals.begin(), literals.end());

		//if existing instance clause was simplified, replace it in UNSAT proof
		replaceExistingClause = RemoveLiteralsDuplicates(literals);
		isSat = CheckTrivialClauseSatisfiability(literals);
		
	}
	
	//only for instance clauses, not for learnt
	if (_solverParams.require_unsat_proof && replaceExistingClause) {
		_drupProofHelper.AddClause(literals);
		_drupProofHelper.RemoveClause(oldLiterals);
	}
	if (isSat)
		return true;
		
	if (_solverParams.require_unsat_proof && fromConflict) {
		_drupProofHelper.AddClause(literals);
	}
	
			
	if (literals.size() == 1) {
		bool assignmentPossible = TryPropagateLiteral(literals[0], nullptr);
		if (assignmentPossible) {
			SharedClauseReference c = BooleanConstraintPropagation();
			if (c == nullptr)
				return true;
			
		}
		return false;
	}

	if (fromConflict) {
		int maxVariableIndex = 1;
		for (unsigned i = 2; i < literals.size(); i++) {
			int variableNo = literals[i].GetVariableNo();
			int maxVariableNo = literals[maxVariableIndex].GetVariableNo();

			if (_variableManager.GetVariableDecisionLevel(variableNo) > _variableManager.GetVariableDecisionLevel(maxVariableNo))
				maxVariableIndex = i;
		}
		swap(literals[1], literals[maxVariableIndex]);
	}

	SharedClauseReference cref = Clause::MakeSharedReference(literals, fromConflict, _clauseid++, clausewidth);
	_variableManager.UpdateClauseVariablesOccurrences(cref, +1);

	if (fromConflict) {
		if (_solverParams.use_weighted_VSIDS_heuristic) {
			bool rescale = _variableManager.WeightedVSIDSBumpConflictVariablesActivity(cref, _currentConflictsCount2, _solverParams.weighted_VSIDS_G, _solverParams.variable_activity_limit_rescale, _solverParams.variable_activity_rescale_factor);
			if (rescale)
				_currentConflictsCount2 = 0;
		}
		else {
			bool rescale = _variableManager.VSIDSBumpConflictVariablesActivity(cref, _solverParams.conflict_variable_activity_increase, _solverParams.variable_activity_limit_rescale, _solverParams.variable_activity_rescale_factor);
			if (rescale)
				_solverParams.conflict_variable_activity_increase *= _solverParams.variable_activity_rescale_factor;
		}

		
		BumpClauseActivity(cref);
		
		bool assignmentPossible = TryPropagateLiteral(literals[0], cref);
		if (!assignmentPossible) {
			RaiseError("TryPropagateLiteral for learnt clause - assignment conflicting! This should not happen");
			return false;
		}
		
		unsigned int clbd = ComputeLBD(cref);
		cref->SetLBD(clbd);
		_learntClauses.push_back(cref);
		_learntClausesCount++;
		_totalLBDSum += ((double)clbd);
		_lbdQueue.Insert(clbd);
		
		BlockRestart();
		
	}
	else {
		//expreiment with no result
		//if ((_solverParams.max_lbd_for_frozen_clause < (literals.size() / 2 + 2)) && (_solverParams.max_lbd_for_frozen_clause < _solverParams.lbd_for_frozen_clause_bound)) {
		//	_solverParams.max_lbd_for_frozen_clause = ((literals.size() / 2 + 2) < _solverParams.lbd_for_frozen_clause_bound ? (literals.size() / 2 + 2) : _solverParams.lbd_for_frozen_clause_bound);
		//}
		
		_clauses.push_back(cref);
	}

	Literal lit0neg = literals[0].Negate();
	Literal lit1neg = literals[1].Negate();

	_watchedLiterals.AddClauseForLiteral(cref, lit0neg.GetUniqueIndex());
	_watchedLiterals.AddClauseForLiteral(cref, lit1neg.GetUniqueIndex());

	cref->SetBlocker(literals[0]);

	ProcessSubsumptionForNewClause(cref);

	return true;
}

void Solver::UncheckedPropagateLiteral(Literal literal, SharedClauseReference conflict) {
	int variableNo = literal.GetVariableNo();
	int currentDecisionLevel = _trail.GetCurrentDecisionLevel();

	this->UpdateAgility(literal);

	_model[variableNo] = (literal.GetSign() == LiteralSign_Positive ? LogicalValue::True : LogicalValue::False);
	_variableManager.SetVariablePolarity(variableNo, literal.GetSign());
	_variableManager.SetVariableAssigned(variableNo);
	_variableManager.SetVariableDecisionLevel(variableNo, currentDecisionLevel);
	_variableManager.SetVariableReason(variableNo, conflict);
	_propagationQueue.PushLiteral(literal);
	_trail.PushLiteral(literal);
	_trailQueue.Insert(_trail.GetSize());
}

// Refining restarts strategies for SAT and UNSAT
void Solver::BlockRestart() {
	if (_conflictsCount < _solverParams.lower_bound_for_blocking_restart)
		return;
	
	if (!_trailQueue.IsFull())
		return;
		
	if (!_lbdQueue.IsFull())
		return;
		
	if (_trail.GetSize() > (_solverParams.R * _trailQueue.Average())) { //  trail size greater than R times the moving average of the last 5000 conflicts trail sizes 
		_lbdQueue.Clear(); // postpone restart
		_blockedRestarts++;
	}
}

void Solver::UpdateAgility(Literal& literal) {
	unsigned oldPolarity = _variableManager.GetVariablePolarity(literal.GetVariableNo());
	unsigned newPolarity = literal.GetSign();

	this->_agility *= _solverParams.magic_agility_decay_factor;
	if ((oldPolarity != newPolarity))
	//if ((oldPolarity != newPolarity) && (oldPolarity != LiteralSign_Unset))
		this->_agility += (1.0 - _solverParams.magic_agility_decay_factor);
}

bool Solver::TryPropagateLiteral(Literal literal, SharedClauseReference conflict) {
	LogicalValue literalValue = this->GetLiteralValue(literal);
	bool conflictingAssignment = false;

	if (literalValue != LogicalValue::None) {
		conflictingAssignment = (literalValue == LogicalValue::False);
		return !conflictingAssignment;
	}

	UncheckedPropagateLiteral(literal, conflict);
	return true;
}

bool Solver::ChangeClauseWatchedLiteral(SharedClauseReference clause, Literal& baseLiteral, Literal& watchedLiteral, Literal& blocker) {
	// invariant: watched literals are always 0 and 1
	// make sure that current watched literal is 1 - if not - swap
	vector<Literal>& clauseLiterals = clause->GetLiterals();

	if (watchedLiteral == clauseLiterals[0])
		swap(clauseLiterals[0], clauseLiterals[1]);

	//if (this->GetLiteralValue(clauseLiterals[0]) == LogicalValue::True) { 
	if (this->GetLiteralValue(clauseLiterals[0]) == LogicalValue::True) { 
		// clause already satisfied
		_watchedLiterals.AddClauseForLiteral(clause, baseLiteral.GetUniqueIndex());
		return false;
	}

	for (unsigned i = 2; i < clauseLiterals.size(); i++) {
		if (this->GetLiteralValue(clauseLiterals[i]) != LogicalValue::False) {
			swap(clauseLiterals[1], clauseLiterals[i]);
			Literal p = clauseLiterals[1].Negate();
			_watchedLiterals.AddClauseForLiteral(clause, p.GetUniqueIndex());
			return false;
		}
	}

	//unit clause - new watched literal not found
	_watchedLiterals.AddClauseForLiteral(clause, baseLiteral.GetUniqueIndex());
	return true;
}

SharedClauseReference Solver::BooleanConstraintPropagation() {
	while (_propagationQueue.Any()) {
		SharedClauseReference conflictClause = nullptr;
		Literal literal = _propagationQueue.RetrieveFirst();
		Literal negatedLiteral = literal.Negate();
		
		vector<SharedClauseReference>& watchingClauses = _watchedLiterals.GetWatchingClausesForLiteral(literal.GetUniqueIndex());
		//_watchedLiterals.ClearWatchingListForLiteral(literal.GetUniqueIndex());
		int currentIndex = watchingClauses.size();
		currentIndex--;

		while (currentIndex >= 0) {
			SharedClauseReference lastClause = watchingClauses[currentIndex];
			currentIndex--;
			
			if (lastClause->IsRemoved()) {
				std::swap(watchingClauses[currentIndex+1], watchingClauses.back());
				watchingClauses.pop_back();
				continue;
			}
			Literal blocker = lastClause->GetBlocker();
			if (this->GetLiteralValue(blocker) == LogicalValue::True) {
				//_watchedLiterals.AddClauseForLiteral(lastClause, literal.GetUniqueIndex());
				_blockersTrue++;
				continue;
			}

			std::swap(watchingClauses[currentIndex+1], watchingClauses.back());
			watchingClauses.pop_back();
			

			
			_propagationsCount++;
			
			bool unitClause = ChangeClauseWatchedLiteral(lastClause, literal, negatedLiteral, blocker);
			if (unitClause) {
				Literal unitLiteral = lastClause->GetLiterals()[0];
				LogicalValue unitLiteralValue = this->GetLiteralValue(unitLiteral);
				bool conflictingAssignment = false;

				if (unitLiteralValue != LogicalValue::None) {
					conflictingAssignment = (unitLiteralValue == LogicalValue::False);
					if (conflictingAssignment) {
						conflictClause = lastClause;
						break;
					}
				}
				else {
					UncheckedPropagateLiteral(unitLiteral, lastClause);
				}
			}
		}

		if (conflictClause != nullptr) {
			//_watchedLiterals.RestoreRemovedClauses(watchingClauses, literal.GetUniqueIndex());
			return conflictClause;
		}
	}

	return Clause::EmptyClause;
}

void Solver::TryFindAssignment() {
	this->SetState(SolverState::InProgress);

	int itercount = 0;
	bool _PLEdone = false;

	while (true) {
		itercount++;

		if (this->ShouldRestart()) {
			this->BeforeRestart();
			return;
		}

		SharedClauseReference conflict = this->BooleanConstraintPropagation();

		if (conflict == nullptr) { // || (conflict->IsEmpty())) {

			if (_variableManager.AllVariablesAssigned()) {
				this->SetState(SolverState::Satisfiable);
				return;
			}

			if (_trail.GetCurrentDecisionLevel() == 0) {
				SolverState ss = this->SimplifyClauseDatabase();
				if (ss == SolverState::Unsatisfiable) {
					this->SetState(SolverState::Unsatisfiable);
					if (_solverParams.require_unsat_proof)
						_drupProofHelper.CloseProof();
					return;
				}
				
				if (!_PLEdone && _solverParams.enable_pure_literal_elimination) {
					_PLEdone = true;
					this->ProcessPureLiteralElimination();
				}
			}

			if (this->ShouldReduceClauseDatabase()) {
				this->ReduceClauseDatabase();
				
				if (_solverParams.use_average_lbd_restart_strategy)
					_learntClausesLimit += _solverParams.reduceDB_conflicts_increment_for_lbd_restarts;
			}

			AssignNextVariable();
		}
		else {
			if (_trail.GetCurrentDecisionLevel() == 0) {
				this->SetState(SolverState::Unsatisfiable);
				if (_solverParams.require_unsat_proof)
					_drupProofHelper.CloseProof();
				return;
			}
			
			HandleConflict(conflict);
			
			AdjustLubyLearntsLimit();
			AdjustTrivialLearntsLimit();

			if (ShouldDecayVariableActivities(_currentConflictsCount)) {
				_solverParams.conflict_variable_activity_increase *= (1.0 / _solverParams.variable_activity_decay_factor);
				//_variableManager.DecayVariablesActivities(_solverParams.variable_activity_decay_factor, _solverParams.variable_activity_limit_rescale, _solverParams.variable_activity_rescale_factor);
			}

			if (ShouldDecayClauseActivities(_currentConflictsCount)) {
				DecayClauseActivities();
			}
		}
	}
	this->SetState(SolverState::InternalError);
	return;
}

void Solver::AdjustLubyLearntsLimit() {
	if (!_solverParams.use_luby_restart_strategy)
		return;

	_lubyCurrentConflictsToAdjustLearntSize -= 1;
	if (_lubyCurrentConflictsToAdjustLearntSize == 0) {
		_lubyConflictsLimitToAdjustLearntSize *= _solverParams.luby_conflicts_increase_factor;
		_lubyCurrentConflictsToAdjustLearntSize = _lubyConflictsLimitToAdjustLearntSize;
		_learntClausesLimit *= _solverParams.learnt_clauses_limit_increase_factor_for_luby_restart;
		//cout << "Luby restarts new learnt clauses limit " << _learntClausesLimit << endl;
	}
}

void Solver::AdjustTrivialLearntsLimit() {
	if (!_solverParams.use_trivial_restarts_strategy)
		return;

	_trivialCurrentConflictsToAdjustLearntSize -= 1;
	if (_trivialCurrentConflictsToAdjustLearntSize == 0) {
		//if (_verbosity >= 1)
		//	cout << "c conflicts current " << _conflictsCount << " propagations " << _propagationsCount << " learnts limit " << _learntClausesLimit << " learnts size " << _learntClauses.size() <<  endl;
			
		_trivialConflictsLimitToAdjustLearntSize *= _solverParams.trivial_conflicts_increase_factor;
		_trivialCurrentConflictsToAdjustLearntSize = (int)_trivialConflictsLimitToAdjustLearntSize;
		_learntClausesLimit *= _solverParams.learnt_clauses_limit_increase_factor_for_trivial_restart;
		//cout << "Luby restarts new learnt clauses limit " << _learntClausesLimit << endl;
	}
}

void Solver::AssignNextVariable() {
	_decisions++;
	int variableNo = _variableManager.GetNextVariableToAssign();
	
	//AssignNextVariableTrue(variableNo);
	//AssignNextVariableFalse(variableNo);
	
	AssignNextVariableByOccurencesAndWatches(variableNo);
	//AssignNextVariableByWatches(variableNo);
	//AssignNextVariableByPhaseSaving(variableNo);
}

void Solver::AssignNextVariableByWatches(int variableNo) {
	
	Literal p = Literal(variableNo, LiteralSign_Positive);
	Literal n = Literal(variableNo, LiteralSign_Negative);

	Literal r = p;

	int pos_w = _watchedLiterals.GetWatchingClausesCountForLiteral(n.GetUniqueIndex());
	int neg_w = _watchedLiterals.GetWatchingClausesCountForLiteral(p.GetUniqueIndex());

	if (pos_w < neg_w)
		r = p;
	else
		r = n;

	_trail.NewDecisionLevel();
	UncheckedPropagateLiteral(r, nullptr);

}

void Solver::AssignNextVariableByOccurences(int variableNo) {
	unsigned polarity = LiteralSign_Unset;
	int pos_oc = _variableManager.GetPositiveOccurencesCount(variableNo);
	int neg_oc = _variableManager.GetNegativeOccurencesCount(variableNo);
		
	if (pos_oc >= neg_oc)
		polarity = LiteralSign_Positive;
	else
		polarity = LiteralSign_Negative;

	Literal r = Literal(variableNo, polarity);
	
	_trail.NewDecisionLevel();
	UncheckedPropagateLiteral(r, nullptr);
	
}

void Solver::AssignNextVariableByOccurencesAndWatches(int variableNo) {
	Literal p = Literal(variableNo, LiteralSign_Positive);
	Literal n = Literal(variableNo, LiteralSign_Negative);
	Literal r = p;

	int pos_w = _watchedLiterals.GetWatchingClausesCountForLiteral(n.GetUniqueIndex());
	int neg_w = _watchedLiterals.GetWatchingClausesCountForLiteral(p.GetUniqueIndex());
	int pos_oc = _variableManager.GetPositiveOccurencesCount(variableNo);
	int neg_oc = _variableManager.GetNegativeOccurencesCount(variableNo);

	int pos_score = pos_w + pos_oc;
	int neg_score = neg_w + neg_oc;

	if (pos_score >= neg_score)
		r = p;
	else
		r = n;

	_trail.NewDecisionLevel();
	UncheckedPropagateLiteral(r, nullptr);
	
}

void Solver::AssignNextVariableByRandom(int variableNo) {
	unsigned sign = ((rand() % 2) == 1) ? LiteralSign_Positive : LiteralSign_Negative;

	Literal r = Literal(variableNo, sign);

	_trail.NewDecisionLevel();
	UncheckedPropagateLiteral(r, nullptr);

}

void Solver::AssignNextVariableByPhaseSaving(int variableNo) {
	unsigned sign = _variableManager.GetVariablePolarity(variableNo);
	if (sign == LiteralSign_Unset) {
		AssignNextVariableFalse(variableNo);
		return;
	}

	Literal r = Literal(variableNo, sign);

	_trail.NewDecisionLevel();
	UncheckedPropagateLiteral(r, nullptr);
}

void Solver::AssignNextVariableTrue(int variableNo) {
	Literal r = Literal(variableNo, LiteralSign_Positive);
	_trail.NewDecisionLevel();
	UncheckedPropagateLiteral(r, nullptr);
}
	
void Solver::AssignNextVariableFalse(int variableNo) {
	Literal r = Literal(variableNo, LiteralSign_Negative);
	_trail.NewDecisionLevel();
	UncheckedPropagateLiteral(r, nullptr);
}

void Solver::HandleConflict(SharedClauseReference conflict) {
	_conflictsCount++;
	//if (_conflictsCount %1000 == 0)
	//	cout << "c conflicts " << _conflictsCount << endl;
	_currentConflictsCount++;
	_currentConflictsCount2++;

	_propagationQueue.Erase();
	vector<Literal> learntClause;
	
	pair<int, int> analyzeresult = TraverseImplicationGraph(conflict, learntClause);
	int backtrackLevel = analyzeresult.first;
	int clausewidth = analyzeresult.second;
	
	CancelAssignments(backtrackLevel);
	
	AddClause(learntClause, true, clausewidth);
}

//<backtrack_level, clause_width>
pair<int, int> Solver::TraverseImplicationGraph(SharedClauseReference conflict, vector<Literal>& learntClause) {
	
	vector<bool> visited(_variablesCount, false);
	int bfs_queue_size = 0;
	int clause_width = 0;
	Literal p = Literal(_variablesCount + 1, LiteralSign_Negative);
	//vector<Literal> reason; - unnecessary - may iterate over conflict literals and skip first literal (asserting) if needed 
	int currentLevel = _trail.GetCurrentDecisionLevel();
	int out_level = 0;

	do {
		//reason.clear();
		BumpClauseActivity(conflict);
		if (_solverParams.use_weighted_VSIDS_heuristic) {
			bool rescale = _variableManager.WeightedVSIDSBumpConflictVariablesActivity(conflict, _currentConflictsCount2, _solverParams.weighted_VSIDS_G, _solverParams.variable_activity_limit_rescale, _solverParams.variable_activity_rescale_factor);
			if (rescale)
				_currentConflictsCount2 = 0;
		}
		else {
			bool rescale = _variableManager.VSIDSBumpConflictVariablesActivity(conflict, _solverParams.conflict_variable_activity_increase, _solverParams.variable_activity_limit_rescale, _solverParams.variable_activity_rescale_factor);
			if (rescale)
				_solverParams.conflict_variable_activity_increase *= _solverParams.variable_activity_rescale_factor;
		}
		vector<Literal>& conflictLiterals = conflict->GetLiterals();
		//CalculateReason(conflictLiterals, reason, p);
		
		unsigned i = 0;
		if (conflictLiterals[i] == p)
			i++;
		
		for(;i<conflictLiterals.size();i++) {
			int variableNo = conflictLiterals[i].GetVariableNo();
			int variableLevel = _variableManager.GetVariableDecisionLevel(variableNo);
			if(!visited[variableNo] && variableLevel > 0) 
			{
				visited[variableNo] = true;
				//_variableManager.BumpVariableActivity(variableNo, _solverParams.conflict_variable_activity_increase);
				if(variableLevel == currentLevel)
					bfs_queue_size++;
				else {
					learntClause.push_back(conflictLiterals[i]);
					//out_level = std::max(out_level, variableLevel); -- turned off - calculated after simplifying learnt clause
				}
			}
		}
		
		if (bfs_queue_size > clause_width)
			clause_width = bfs_queue_size;

		if (conflict->IsLearnt()) {
			unsigned int clbd = this->ComputeLBD(conflict);
			
			//if (clbd <= 7)
				//clbd7++;
			//else if (clbd <= 10)
				//clbd10++;
			//else if (clbd <= 15)
				//clbd15++;
			//else if (clbd <= 20)
				//clbd20++;
			//else if (clbd <= 30)
				//clbd30++;
			
			if (clbd <= _solverParams.max_lbd_for_frozen_clause)
				conflict->SetFrozen(true);
				
			//if (clbd + 1 < conflict->GetLBD())
				conflict->SetLBD(clbd);			
		}

		conflict = GetLastVisitedVariableReason(p, visited);
		bfs_queue_size--;
		
	} while(bfs_queue_size > 0);

	//bfs_queue_size == 0 - 1-UIP found

	learntClause.push_back(p.Negate());
	swap(learntClause.front(), learntClause.back());

	this->SimplifyLearntClause(learntClause, visited);

	out_level = CalculateBacktrackLevelForLearntClause(learntClause);

	return std::make_pair(out_level, clause_width);
}

SharedClauseReference Solver::GetLastVisitedVariableReason(Literal& p, vector<bool>& visited) {
	SharedClauseReference conflict = nullptr;
	int trailIndex = _trail.GetSize() - 1;
	Literal tl = _trail.At(trailIndex);
	
	while(!visited[tl.GetVariableNo()]) {
		trailIndex--;
		tl = _trail.At(trailIndex);
	}
	
	p = tl;
	visited[p.GetVariableNo()] = false;
	conflict = _variableManager.GetVariableReason(p.GetVariableNo());
	return conflict;
}

//void Solver::CalculateReason(vector<Literal>& conflictLiterals, vector<Literal>& reason, Literal p) {
	//for (unsigned i = 0; i<conflictLiterals.size(); i++) {
		//if (conflictLiterals[i].GetUniqueIndex() != p.GetUniqueIndex())
			//reason.push_back(conflictLiterals[i]);
	//}
//}

unsigned int Solver::ComputeLBD(SharedClauseReference clause) {
	vector<bool> levels(_variablesCount+2, false); // level may be -1
	int lbd = 0;

	vector<Literal>& literals = clause->GetLiterals();

	for (unsigned i = 0; i < literals.size();i++) {
		int variableno = literals[i].GetVariableNo();
		int variablelevel = _variableManager.GetVariableDecisionLevel(variableno);
		if (variablelevel < 0)
			continue;
		if (levels[variablelevel+1] == false) {
			levels[variablelevel+1] = true;
			lbd++;
		}
	}

	return lbd;
}

bool Solver::IsClauseLocked(SharedClauseReference clause) {
	// tylko ten moze byc zawartosciowany przez te klauzule jesli klauzula byla unit i dostal wartosc w wyniku UP
	Literal possibleUnitLiteral = clause->GetLiterals()[0]; 

	Clause* reason_ptr = _variableManager.GetVariableReason(possibleUnitLiteral.GetVariableNo()).get();
	Clause* clause_ptr = clause.get();

	return reason_ptr == clause_ptr;

}

void Solver::SimplifyLearntClause(vector<Literal>& learnt_clause, vector<bool>& visited) {
	if (_solverParams.clause_minimization_strategy == 1)
		SimplifyLearntClauseSimple(learnt_clause, visited);
	else if (_solverParams.clause_minimization_strategy == 2)
		SimplifyLearntClauseRecursive(learnt_clause, visited);
}

void Solver::SimplifyLearntClauseSimple(vector<Literal>& learnt_clause, vector<bool>& visited) {
	unsigned current_index = 1;
	unsigned current_clause_position = 1;

	unsigned old_size = learnt_clause.size();
	
	for (; current_index < learnt_clause.size(); current_index++) {
		int variableNo = learnt_clause[current_index].GetVariableNo();
		SharedClauseReference this_reason = _variableManager.GetVariableReason(variableNo);
		if (this_reason == nullptr) {
			learnt_clause[current_clause_position] = learnt_clause[current_index];
			current_clause_position++;
		}
		else {
			//if all variables included in this variable reason were visited - then do not include this variable in conflict clause
			bool include_current_literal = false;
			
			vector<Literal>& reasonLiterals = this_reason->GetLiterals();
			for (unsigned i = 1; i < reasonLiterals.size(); i++) {
				int currentVariableNo = reasonLiterals[i].GetVariableNo();
				if (_variableManager.GetVariableDecisionLevel(currentVariableNo) == 0)
					continue;

				if (!visited[currentVariableNo]) {
					include_current_literal = true;
					break;
				}
			}
			if (include_current_literal) {
				learnt_clause[current_clause_position] = learnt_clause[current_index];
				current_clause_position++;
			}
		}
	}
	learnt_clause.resize(current_clause_position);
	unsigned new_size = learnt_clause.size();
	if (new_size < old_size) {
		_learntClauseReductionsCount++;
		
		double difference = (double)(old_size - new_size);
		difference = difference / old_size;
		_learntClauseReductionsSum += difference;
		//cout << "Learnt clause reduced from " << old_size << " to " << new_size << endl;

	}
}

void Solver::SimplifyLearntClauseRecursive(vector<Literal>& learnt_clause, vector<bool>& visited) {
	vector<Literal> recursion_stack;
	vector<int> clear_visited_variables_onerror; // allocated once to avoid reallocs
	recursion_stack.reserve(128);
	clear_visited_variables_onerror.reserve(128);
	
	unsigned old_size = learnt_clause.size();
	
	unsigned int base_conflict_visited_levels = 0;
	for (unsigned i = 1; i < learnt_clause.size(); i++) {
		int variableNo = learnt_clause[i].GetVariableNo();
		base_conflict_visited_levels |= _variableManager.GetVariableDecisionLevelHash(variableNo);
	}
	
	unsigned current_index = 1;
	unsigned current_clause_position = 1;
	
	for (; current_index < learnt_clause.size(); current_index++) {
		int variableNo = learnt_clause[current_index].GetVariableNo();
		SharedClauseReference this_reason = _variableManager.GetVariableReason(variableNo);
		bool include_current_literal = false;
		if (this_reason == nullptr) // decision vertex
			include_current_literal = true;
		else if (!IsLiteralDomiated(learnt_clause[current_index], base_conflict_visited_levels, recursion_stack, clear_visited_variables_onerror, visited)) // must include literal - not dominated
			include_current_literal = true;
		
		
		if (include_current_literal) {
			learnt_clause[current_clause_position] = learnt_clause[current_index];
			current_clause_position++;
		}
	}
	
	learnt_clause.resize(current_clause_position);
	unsigned new_size = learnt_clause.size();
	if (new_size < old_size) {
		_learntClauseReductionsCount++;
		
		double difference = (double)(old_size - new_size);
		difference = difference / old_size;
		_learntClauseReductionsSum += difference;
		//cout << "Learnt clause reduced from " << old_size << " to " << new_size << endl;
	}
}

bool Solver::IsLiteralDomiated(Literal& p, unsigned int base_conflict_visited_levels, vector<Literal>& recursion_stack, vector<int>& clear_visited_variables_onerror, vector<bool>& visited) {
    recursion_stack.clear();
    recursion_stack.push_back(p);
    
    while (recursion_stack.size() > 0){
		Literal lastLit = recursion_stack.back();
		recursion_stack.pop_back();
		SharedClauseReference last_reason = _variableManager.GetVariableReason(lastLit.GetVariableNo());
        
        vector<Literal>& last_literals = last_reason->GetLiterals();

        for (unsigned i = 1; i < last_literals.size(); i++){
            Literal p  = last_literals[i];
            int variableNo = p.GetVariableNo();
            int variableLevel = _variableManager.GetVariableDecisionLevel(variableNo);
            SharedClauseReference variableReason = _variableManager.GetVariableReason(variableNo);
            
            if (!visited[variableNo] && variableLevel > 0){
                if (variableReason != nullptr && (_variableManager.GetVariableDecisionLevelHash(variableNo) & base_conflict_visited_levels) != 0){
                    visited[variableNo] = true;
                    recursion_stack.push_back(p);
                    clear_visited_variables_onerror.push_back(variableNo);
                } 
                else{
                    for (unsigned j = 0; j < clear_visited_variables_onerror.size(); j++)
                        visited[clear_visited_variables_onerror[j]] = false;
                    clear_visited_variables_onerror.clear(); // clear before return 
                    return false;
                }
            }
        }
    }

    return true;
}

int Solver::CalculateBacktrackLevelForLearntClause(vector<Literal>& learnt_clause) {
	int root_level = 0;
	int backtrack_level = 0;
	int best_index = 1;

	if (learnt_clause.size() == 1)
		return root_level;

	for (unsigned i = 2; i < learnt_clause.size(); i++) {
		int current_var_level = _variableManager.GetVariableDecisionLevel(learnt_clause[i].GetVariableNo());
		int max_level = _variableManager.GetVariableDecisionLevel(learnt_clause[best_index].GetVariableNo());
		if (current_var_level > max_level)
			best_index = i;
	}

	swap(learnt_clause[1], learnt_clause[best_index]);
	backtrack_level = _variableManager.GetVariableDecisionLevel(learnt_clause[1].GetVariableNo());

	return backtrack_level;
}

bool Solver::ShouldReduceClauseDatabase() {
	//int assigned_count = _variableManager.GetAssignedVariablesCount();
	//int lcsize = (int)_learntClauses.size();
	
	//if (lcsize - assigned_count >= _learntClausesLimit)
	//	cout << "c ass " << _learntClauses.size() - assigned_count << " " << _learntClausesLimit  << endl;
	
	return (_learntClauses.size() >= _learntClausesLimit);
	//return (lcsize - assigned_count >= _learntClausesLimit);
}

void Solver::SortLearntClauses() {
	if (_solverParams.use_clause_activity_comparer) {
		ClauseActivityComparer cmp = ClauseActivityComparer(_solverParams.max_small_clause_size);
		std::sort(_learntClauses.begin(), _learntClauses.end(), cmp);
		
		//cout << "ac deb " << _learntClauses[0]->GetActivity() << " " << _learntClauses[_learntClauses.size()/2]->GetActivity()  << " " << _learntClauses[_learntClauses.size()-1]->GetActivity() << endl;
		
	}
	else if (_solverParams.use_clause_width_comparer) {
		ClauseWidthComparer cmp = ClauseWidthComparer(_solverParams.max_small_clause_size);
		std::sort(_learntClauses.begin(), _learntClauses.end(), cmp);
		
	}
	else if (this->_useSizeBasedClauseComparer) {
		ClauseSizeComparer cmp = ClauseSizeComparer(_solverParams.max_small_clause_size);
		std::sort(_learntClauses.begin(), _learntClauses.end(), cmp);
	}
	else {
		ClauseLBDComparer cmp = ClauseLBDComparer(_solverParams.max_small_clause_size);
		std::sort(_learntClauses.begin(), _learntClauses.end(), cmp);
	}
}

void Solver::ReduceClauseDatabase() {
	_databaseReductionsCount++;
	
	//cout << "reduce database " << _learntClausesLimit << endl;

	SortLearntClauses();
	
	int leftClausesCount = 0;
	unsigned toRemoveCount = _learntClauses.size() / 2;
	
	double  min_activity_limit = _solverParams.conflict_clause_activity_increase / _learntClauses.size(); 
	
	//experiment with no result
	//unsigned halfIndex = (toRemoveCount * 2) / 3;
	//_halfClauseLBD = _learntClauses[halfIndex]->GetLBD();

	//if (_learntClauses[toRemoveCount]->GetLBD() <= 5) {
		//_learntClausesLimit += (2 * _variablesCount);
		//toRemoveCount -= (2 * _variablesCount);
	//}
	//else if (_learntClauses[toRemoveCount]->GetLBD() <= 7) {
		//_learntClausesLimit += _variablesCount;
		//toRemoveCount -= _variablesCount;
	//}

	for (unsigned i = 0; i < _learntClauses.size(); i++) {
		SharedClauseReference cc = _learntClauses[i];
		bool removeThis = true;

		if (cc->LiteralsCount() <= _solverParams.max_small_clause_size)
			removeThis = false;
		if (removeThis && (i > toRemoveCount)) {
			removeThis = false;
		}
		if (cc->GetActivity() <= min_activity_limit) // always remove
			removeThis = true;

		if (removeThis && !cc->IsFrozen() && !IsClauseLocked(cc)) {
			RemoveClause(_learntClauses[i]);
		} 
		else {
			if (cc->IsFrozen()) {
				cc->SetFrozen(false);
				toRemoveCount++;
			}
			_learntClauses[leftClausesCount] = cc;
			leftClausesCount++;
		}
	}
	_learntClauses.resize(leftClausesCount);
}


SolverState Solver::SimplifyClauseDatabase() {
	//if (true)
	//	return SolverState::Unknown;
	
	vector<Literal> removedLiterals;
	
	for (int i = 0; i < _clauses.size(); i++) {
		SharedClauseReference cref = _clauses[i];
		removedLiterals.clear();
		
		bool toDelete = cref->Simplify(_model, removedLiterals);
		
		for (unsigned j=0;j<removedLiterals.size();j++) {
			int variableNo = removedLiterals[j].GetVariableNo();
			bool positive = (removedLiterals[j].GetSign() == LiteralSign_Positive);
			_variableManager.UpdateVariableOccurrences(variableNo, positive, -1);
		}
		
		
		if (toDelete) {
			std::swap(_clauses[i], _clauses.back());
			RemoveClause(_clauses.back());
			_clauses.pop_back();
			i--;
			_deletedClausesCount++;
		}
		else if (cref->LiteralsCount() == 0) { // unsatisfiable clause
			//RaiseError("Empty clause after simplification - this should not happen");
			
			return SolverState::Unsatisfiable;
		}
		else if (cref->LiteralsCount() == 1) {
			Literal lit = cref->GetLiterals()[0];
			if (TryPropagateLiteral(lit, nullptr) == false) {
				RaiseError("Unit clause after simplification - propagation error - this should not happen");
				return SolverState::Unsatisfiable;
			}
			std::swap(_clauses[i], _clauses.back());
			RemoveClause(_clauses.back());
			_clauses.pop_back();
			i--;
			
			_unitSimplifiedClausesCount++;
		}
	}

	for (int i = 0; i < _learntClauses.size(); i++) {
		SharedClauseReference cref = _learntClauses[i];
		removedLiterals.clear();
		
		bool toDelete = cref->Simplify(_model, removedLiterals);
		
		for (unsigned j=0;j<removedLiterals.size();j++) {
			int variableNo = removedLiterals[j].GetVariableNo();
			bool positive = (removedLiterals[j].GetSign() == LiteralSign_Positive);
			_variableManager.UpdateVariableOccurrences(variableNo, positive, -1);
		}
		
		if (toDelete) {
			std::swap(_learntClauses[i], _learntClauses.back());
			RemoveClause(_learntClauses.back());
			_learntClauses.pop_back();
			i--;
			_deletedClausesCount++;
		}
		else if (cref->LiteralsCount() == 0) { // unsatisfiable clause
			//RaiseError("Empty clause after simplification - this should not happen");
			return SolverState::Unsatisfiable;
		}
		else if (cref->LiteralsCount() == 1) {
			Literal lit = cref->GetLiterals()[0];
			if (TryPropagateLiteral(lit, nullptr) == false) {
				RaiseError("Unit clause after simplification - propagation error - this should not happen");
				return SolverState::Unsatisfiable;
			}
			std::swap(_learntClauses[i], _learntClauses.back());
			RemoveClause(_learntClauses.back());
			_learntClauses.pop_back();
			i--;
			_unitSimplifiedClausesCount++;
		}
	}

	return SolverState::InProgress;
}

void Solver::CancelAssignments(int backtrackLevel) {
	while (backtrackLevel < _trail.GetCurrentDecisionLevel()) {
		Literal lastLit = _trail.RetrieveLast();
		_variableManager.RemoveAssignment(lastLit.GetVariableNo());
		_model[lastLit.GetVariableNo()] = LogicalValue::None;
	}
}

bool Solver::ShouldDecayVariableActivities(int conflictNo) {
	//return (conflictNo % 8) == 0;
	return true;
}

bool Solver::ShouldDecayClauseActivities(int conflictNo) {
	//return (conflictNo % 128) == 0;
	return true;
}

bool Solver::ShouldRestart() {
	if (_solverParams.use_agility_based_restarts_strategy)
	{
		//return _agility < _solverParams.restart_agility_limit;
		if ((_agility < _solverParams.restart_agility_limit) && (_currentConflictsCount > _conflictsLimitForAgilityRestart))
			return true;

		if ((_agility < _solverParams.force_restart_agility_limit) && (_currentConflictsCount > (_conflictsLimitForAgilityRestart / 2)))
			return true;

		return false;
	}

	else if (_solverParams.use_luby_restart_strategy)
	{
		if (_currentConflictsCount > _conflictsLimitForLubyRestart)
			return true;

		return false;
	}
	else if (_solverParams.use_average_lbd_restart_strategy)
	{
		if (!_lbdQueue.IsFull()) // handle not less than queuesize conflicts between restarts
			return false;
		if (_totalLBDSum < 1) // avoid dividing by zero
			return false;
			
		double total_average = _totalLBDSum / _conflictsCount;
		double queue_average = _lbdQueue.Average();
		
		return ((queue_average * _solverParams.K) > total_average); // average over last queuesize LBDs become too big wrt total average
	}
	else if (_solverParams.use_trivial_restarts_strategy) {
		if (_currentConflictsCount > _conflictsLimitForTrivialRestart)
			return true;

		return false;
	}
	else
		return false;
}

void Solver::BeforeRestart() {
	this->SetState(SolverState::Restart);
}

void Solver::OnRestart() {
	int root_level = 0;
	
	//_decisions = 0;
	_restartsCount++;
	_agility = 1.0;
	_currentConflictsCount = 0;
	CancelAssignments(root_level);
	_propagationQueue.Erase();
	
	//experiment with no result
	//if ((_solverParams.max_lbd_for_frozen_clause < _halfClauseLBD) && (_halfClauseLBD <= _solverParams.lbd_for_frozen_clause_bound)) {
	//	_solverParams.max_lbd_for_frozen_clause = _halfClauseLBD;
	//}
	//cout << "clbd7 " << clbd7 << endl;
	//cout << "clbd10  " << clbd10 << endl;
	//cout << "clbd15  " << clbd15 << endl;
	//cout << "clbd20  " << clbd20 << endl;
	//cout << "clbd30  " << clbd30 << endl;
	//cout << "_learntClausesLimit " << _learntClausesLimit << endl;
	//cout << "--------------------" << endl;
	
	//clbd7 = clbd10 = clbd15 = clbd20 = clbd30 = 0;

	if (_solverParams.use_agility_based_restarts_strategy) {
		_conflictsLimitForAgilityRestart *= _solverParams.conflicts_limit_increase_factor_for_agility_restart;
		_learntClausesLimit *= _solverParams.learnt_clauses_limit_increase_factor_for_agility_restart;
	}
	else if (_solverParams.use_luby_restart_strategy) {
		_conflictsLimitForLubyRestart = Luby(_restartsCount);
		//cout << "Luby conf limit at restart " << _restartsCount << " is " << _conflictsLimitForLubyRestart << " learnt limit is " << _learntClausesLimit << endl;
	}
	else if (_solverParams.use_trivial_restarts_strategy) {
		_conflictsLimitForTrivialRestart *= 1.5;
	}
	
	//lbd restarts
	_lbdQueue.Clear();
}

double Solver::Luby(int restartsCount) {
	// Find the finite subsequence that contains index 'restartsCount', and the
	// size of that subsequence:
	int sequence_size = 1;
	int seq = 0;
	for (; sequence_size <= restartsCount; sequence_size = 2 * sequence_size + 1)
		seq++;

	while (sequence_size - 1 != restartsCount) {
		sequence_size = (sequence_size - 1) / 2;
		seq--;
		restartsCount = restartsCount % sequence_size;
	}

	return _solverParams.luby_unit_factor * pow(_solverParams.luby_power_factor, seq);
}

void Solver::DecayClauseActivities() {
	_solverParams.conflict_clause_activity_increase *= (1.0 / _solverParams.clause_activity_decay_factor);
	// bool do_rescale = false;

	// for (unsigned i = 0; i < _learntClauses.size(); i++) {
	// 	_learntClauses[i]->RescaleActivity(_solverParams.clause_activity_decay_factor);
	// 	if (_learntClauses[i]->GetActivity() > _solverParams.clause_activity_limit_rescale)
	// 		do_rescale = true;
	// }

	// if (!do_rescale)
	// 	return;

	// for (unsigned i = 0; i < _learntClauses.size(); i++) {
	// 	_learntClauses[i]->RescaleActivity(_solverParams.clause_activity_rescale_factor);
	// }
}

void Solver::ProcessSubsumptionForNewClause(SharedClauseReference cref) {
	if (!_solverParams.enable_clause_subsumption)
		return;
	if (cref->IsLearnt()) {
		if (!_solverParams.enable_clause_subsumption_for_learnt_clauses)
			return;
	}
	else {
		if (!_solverParams.enable_clause_subsumption_for_instance_clauses)
			return;
	}

	unsigned int i = 0;
	if (cref->IsLearnt()) {
		while (i < _learntClauses.size()) {
			if (cref->Subsumes(_learntClauses[i])) {
				_successfulSubsumptionsCount++;
				std::swap(_learntClauses[i], _learntClauses[_learntClauses.size() - 1]);
				RemoveClause(_learntClauses.back());
				_learntClauses.pop_back();
			}
			else {
				if (_solverParams.enable_clause_strenghten) {
					int sIndex = _learntClauses[i]->CanBeStrenghten(cref);
					if (sIndex != -1)
						StrenghtenClause(_learntClauses[i], sIndex);
				}

				i++;
			}
		}
		if (!_solverParams.check_instance_clauses_subsumption_for_learnt_clause)
			return;
	}
	
	i = 0;
	while (i < _clauses.size()) {
		if (cref->Subsumes(_clauses[i])) {
			_successfulSubsumptionsCount++;
			std::swap(_clauses[i], _clauses[_clauses.size() - 1]);
			RemoveClause(_clauses.back());
			_clauses.pop_back();
		}
		else {
			if (_solverParams.enable_clause_strenghten) {
				int sIndex = _clauses[i]->CanBeStrenghten(cref);
				if (sIndex != -1)
					StrenghtenClause(_clauses[i], sIndex);
			}

			i++;
		}
	}
	
}

void Solver::StrenghtenClause(SharedClauseReference cref, int litIndex) {
	vector<Literal> oldliterals = cref->GetLiterals();
	Literal lit = oldliterals[litIndex];
	int variableNo = lit.GetVariableNo();
	bool positive = (lit.GetSign() == LiteralSign_Positive);
	
	cref->Strenghten(litIndex);
	_variableManager.UpdateVariableOccurrences(variableNo, positive, -1);
	_successfulStrenghtenClausesCount++;
	
	if (_solverParams.require_unsat_proof) {
		_drupProofHelper.AddClause(cref->GetLiterals());
		_drupProofHelper.RemoveClause(oldliterals);
	}
}

void Solver::ProcessPureLiteralElimination() {
	if (!_solverParams.enable_pure_literal_elimination)
		return;
	
	for (int i=0;i< _variablesCount; i++) {
		if (_model[i] != LogicalValue::None)
			continue;
		
		int pos_oc = _variableManager.GetPositiveOccurencesCount(i);
		Literal pos_lit = Literal(i, LiteralSign_Positive);
		
		int neg_oc = _variableManager.GetNegativeOccurencesCount(i);
		Literal neg_lit = Literal(i, LiteralSign_Negative);
		
		if (neg_oc <= 0) { // positive literal is pure
			UncheckedPropagateLiteral(pos_lit, nullptr);
			_pureliteralPropagations++;
		}
		else if (pos_oc <= 0) { // negative literal is pure
			UncheckedPropagateLiteral(neg_lit, nullptr);
			_pureliteralPropagations++;
		}
	}
}

void Solver::BumpClauseActivity(SharedClauseReference cref) {
	if (!cref->IsLearnt())
		return;
		
		
	cref->BumpActivity(_solverParams.conflict_clause_activity_increase);
	if (cref->GetActivity() > _solverParams.clause_activity_limit_rescale) {
		for (unsigned i=0;i<_learntClauses.size();i++)
			_learntClauses[i]->RescaleActivity(_solverParams.clause_activity_rescale_factor);
		
		_solverParams.conflict_clause_activity_increase *= _solverParams.clause_activity_rescale_factor;

	}
}

void Solver::RemoveClause(SharedClauseReference cref) {
	if (cref->IsRemoved())
		return;
	
	cref->SetRemoved();
	_variableManager.UpdateClauseVariablesOccurrences(cref, -1);
	
	if (_solverParams.require_unsat_proof && cref->IsLearnt())
		_drupProofHelper.RemoveClause(cref->GetLiterals());
}

void Solver::CheckModelCorrectness() {
	if (this->GetState() == SolverState::Unsatisfiable)
		return;
	
	if (this->GetState() != SolverState::Satisfiable)
		RaiseError("CheckModelCorrectness - incorrect solver state");
	
	for (unsigned i=0;i< _model.size();i++) {
		if (_model[i] == LogicalValue::None) {
			string message = "CheckModelCorrectness - no value for variable ";
			message += std::to_string(i);
			RaiseError(message);
			
		}
	}
	
	for (unsigned i=0;i<_clauses.size();i++) {
		if (!_clauses[i]->IsSatisfied(_model)) {
			string message = "CheckModelCorrectness - clause is not satisfied ";
			message += std::to_string(i);
			_clauses[i]->Print();
			RaiseError(message);
		}
	}
}

void Solver::PrintStats() {
	if (_verbosity >= 1) {
		cout << "c Clauses count " << _clauses.size() << endl;
		cout << "c Learnt clauses count " << _learntClausesCount << endl;
		cout << "c Reduced learnt clauses ratio " << (((double)_learntClauseReductionsCount) /_learntClausesCount) << endl;
		cout << "c Deleted clauses count " << _deletedClausesCount << endl;
		cout << "c Restarts count " << _restartsCount << endl;
		cout << "c Blocked restarts count " << _blockedRestarts << endl;
		cout << "c Conflicts count " << _conflictsCount << endl;
		cout << "c Decisions count " << _decisions << endl;
		cout << "c Propagations count " << _propagationsCount << endl;
		cout << "c Database compactions count " << _databaseReductionsCount << endl;
		cout << "c Learnt clause successful reductions count " << _learntClauseReductionsCount << endl;
		cout << "c Learnt clause reductions global effectiveness " << (_learntClauseReductionsSum / _learntClauseReductionsCount) << endl;
		cout << "c Unit clauses after level 0 simplification count " << _unitSimplifiedClausesCount << endl;
		cout << "c Successful subsumptions count " << _successfulSubsumptionsCount << endl;
		cout << "c Successful strenghten clauses count " << _successfulStrenghtenClausesCount << endl;
		cout << "c Pure literal eliminations count " << _pureliteralPropagations << endl;
		cout << "c Used blockers to skip propagation " << _blockersTrue << endl;
		
		//cout << "clbd7 " << clbd7 << endl;
		//cout << "clbd10  " << clbd10 << endl;
		//cout << "clbd15  " << clbd15 << endl;
		//cout << "clbd20  " << clbd20 << endl;
		//cout << "clbd30  " << clbd30 << endl;
	}
}

void Solver::PrintResult() {
	PrintStats();

	if (this->GetState() == SolverState::Unsatisfiable) {
		cout << "s UNSATISFIABLE" << endl;
		return;
	}
	
	if (this->GetState() != SolverState::Satisfiable)
		RaiseError("PrintResult - incorrect solver state");

	cout << "s SATISFIABLE" << endl;

	if (_solverParams.show_model) {
		cout << "v ";
		for (int i=0;i< (int)_model.size();i++) {
			if (_model[i] == LogicalValue::True)
				cout << i+1 << " ";
			if (_model[i] == LogicalValue::False)
				cout << -(i+1) << " ";
			if (_model[i] == LogicalValue::None)
				cout << "(NONE)" << i+1 << " ";
		}
		cout << 0 << endl;
	}
	
}

void Solver::SetClauseSortingStrategy() {
	unsigned int max_size = 0;

	for (unsigned i = 0; i < _clauses.size(); i++) {
		if (_clauses[i]->LiteralsCount() > max_size)
			max_size = _clauses[i]->LiteralsCount();
	}
	if (max_size <= _solverParams.max_clause_size_for_size_based_comparer)
		_useSizeBasedClauseComparer = true;
}

void Solver::SetLubyUnitFactor() {
	if (_variablesCount <= 10000)
		_solverParams.luby_unit_factor = 100.0;
	else
		_solverParams.luby_unit_factor = 256.0;
}

void Solver::CheckConfigurationErrors() {
	if (_solverParams.use_agility_based_restarts_strategy && _solverParams.use_luby_restart_strategy)
		RaiseError("Only one of restart strategies may be chosen");
}

void Solver::RaiseError(string errorMessage) {
	cerr << errorMessage << endl;
	throw errorMessage;
}
