#ifndef PARSER_H
#define PARSER_H

#include "ParseResult.hpp"
#include <sstream>

class CNFParser {
  public:
	CNFParser();
	
	ParseResult ParseCNFFormula(const char* filePath); 
	
	ParseResult ParseCNFFormulaFromStandardInput();
};


#endif
