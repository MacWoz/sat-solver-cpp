#include "ParseResult.hpp"

ParseResult::ParseResult(int variablesCount, int clausesCount) : _variablesCount(variablesCount), _clausesCount(clausesCount), _error(false), Clauses(clausesCount) {}
	
int ParseResult::GetVariablesCount() {
	return _variablesCount;
}

int ParseResult::GetClausesCount() {
	return _clausesCount;
}
	
bool ParseResult::Failed() {
	return _error;
}
	
string ParseResult::GetErrorMessage() {
	return _errorMessage;
}

void ParseResult::SetError(string message) {
	_error = true;
	_errorMessage = message;
}
