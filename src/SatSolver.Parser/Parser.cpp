#include "Parser.hpp"

CNFParser::CNFParser() {}

ParseResult CNFParser::ParseCNFFormula(const char* filePath) {
	std::ifstream file;
	file.open(filePath);
	if (!file.good()) {
		ParseResult r = ParseResult(0, 0);
		std::string message = "Could not open input file ";
		message.append(filePath);
		r.SetError(message);
		return r;
	}
	
	bool formula_found = false;
	int variablesCount = 0;
	int clausesCount = 0;
	
	string current_line;
	while(std::getline(file, current_line)) {
		if (current_line.find("c") == 0)
			continue;
		if(current_line.find("p cnf") == 0) {
			formula_found = true;
			stringstream ss = stringstream(current_line);
			string p;
			ss >> p;
			ss >> p;
			ss >> variablesCount;
			ss >> clausesCount;
			break;
		}
	}
	
	if (!formula_found) {
		ParseResult r = ParseResult(0, 0);
		r.SetError("CNF formula not found");
		return r;
	}
	/*
	file >> variablesCount;
	file >> clausesCount;
	*/
	ParseResult result = ParseResult(variablesCount, clausesCount);
	
	for (int i=0; i<clausesCount; i++) {
		vector<Literal> current_literals;
		int variableNo;
		while (file >> variableNo) {
			if (variableNo == 0) // clause end
				break;
			unsigned sign = LiteralSign_Positive;
			if (variableNo < 0) {
				variableNo = -variableNo;
				sign = LiteralSign_Negative;
			}
			--variableNo;
			current_literals.push_back(Literal(variableNo, sign));
		}
		result.Clauses[i] = current_literals;
	}
	
	return result; 
		
}

ParseResult CNFParser::ParseCNFFormulaFromStandardInput() {
	//cout << "Parser here 1" << endl;
	
	int variablesCount = 0;
	int clausesCount = 0;
	
	string current_line;
	while(std::getline(cin, current_line)) {
		//cout << "Parser here 1" << current_line << endl;
		if (current_line.find("c") == 0)
			continue;
		if(current_line.find("p cnf") == 0) {
			//formula_found = true;
			stringstream ss = stringstream(current_line);
			string p;
			ss >> p;
			ss >> p;
			ss >> variablesCount;
			ss >> clausesCount;
			//cout << "break " << variablesCount << " " << clausesCount << endl;
			break;
		}
	}
	
	//cin >> variablesCount;
	//cin >> clausesCount;
	
	ParseResult result = ParseResult(variablesCount, clausesCount);
	
	for (int i=0; i<clausesCount; i++) {
		vector<Literal> current_literals;
		int variableNo;
		while (cin >> variableNo) {
			if (variableNo == 0) // clause end
				break;
			unsigned sign = LiteralSign_Positive;
			if (variableNo < 0) {
				variableNo = -variableNo;
				sign = LiteralSign_Negative;
			}
			--variableNo;
			current_literals.push_back(Literal(variableNo, sign));
		}
		result.Clauses[i] = current_literals;
	}
	
	//cout << "return " << variablesCount << " " << clausesCount << endl;
	
	return result; 
	
}
