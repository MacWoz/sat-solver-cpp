#ifndef PARSERESULT_H
#define PARSERESULT_H_

#include "../SatSolver.Types/Literal.hpp"

using namespace std;

class ParseResult {
  private:
	int _variablesCount;
	int _clausesCount;
	bool _error;
	string _errorMessage;
	
	
  public:
	ParseResult(int variablesCount, int clausesCount);
	
	void SetError(string message);
	
	vector<vector<Literal>> Clauses;
		
	int GetVariablesCount();
	
	int GetClausesCount();
	
	bool Failed();
	
	string GetErrorMessage();
	

};


#endif // PARSERESULT_H_
