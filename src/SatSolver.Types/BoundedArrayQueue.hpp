#ifndef BOUNDEDARRAYQUEUE_H
#define BOUNDEDARRAYQUEUE_H

#include "Literal.hpp"


class BoundedArrayQueue {
	
private:

	int _capacity;
	int _size;
	int _nextIndex;
	long long _sum;
	
	vector<int> _data;


public:
	BoundedArrayQueue(int capacity);
	
	void Insert(int value);
	
	double Average();
	
	bool IsFull();
	
	void Clear();
	
	
};

#endif
