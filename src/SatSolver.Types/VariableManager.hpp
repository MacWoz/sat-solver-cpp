#ifndef VARIABLEMANAGER_H
#define VARIABLEMANAGER_H

#include "Variable.hpp"
#include "VariableOrderHeap.hpp"
#include "Clause.hpp"

class VariableManager {
private:
	int _variablesCount;
	int _freeVariablesCount;
	vector<Variable> _variables;
	vector<double> _variablesActivity;
	vector<bool> _variablesFree;
	VariableOrderHeap _heap;

	void SetVariableFree(int variableNo);

public:
	VariableManager(int variablesCount);

	bool AllVariablesAssigned();
	
	int GetAssignedVariablesCount();

	void SetVariableAssigned(int variableNo);

	bool VSIDSBumpConflictVariablesActivity(SharedClauseReference clause, double val, double activity_limit, double activity_rescale_factor);

	bool WeightedVSIDSBumpConflictVariablesActivity(SharedClauseReference clause, int conflictNo, double g, double activity_limit, double activity_rescale_factor);

	void BumpVariableActivity(int variableNo, double val);

	//void RescaleVariableActivity(int variableNo, double factor);

	void SetVariablePolarity(int variableNo, unsigned polarity);

	unsigned GetVariablePolarity(int variableNo);

	void SetVariableDecisionLevel(int variableNo, int decisionLevel);

	int GetVariableDecisionLevel(int variableNo);
	
	unsigned int GetVariableDecisionLevelHash(int variableNo);

	void SetVariableReason(int variableNo, SharedClauseReference reason);

	SharedClauseReference GetVariableReason(int variableNo);

	int GetNextVariableToAssign();

	//Literal GetNextLiteralToAssign();

	void RemoveAssignment(int variableNo);

	//void DecayVariablesActivities(double activity_decay_factor, double activity_limit_rescale, double activity_rescale_factor);

	void UpdateClauseVariablesOccurrences(SharedClauseReference clause, int value);
	
	void UpdateVariableOccurrences(int variableNo, bool positive, int value);

	int GetPositiveOccurencesCount(int variableNo);

	int GetNegativeOccurencesCount(int variableNo);

};


#endif
