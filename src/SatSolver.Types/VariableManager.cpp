
#include "VariableManager.hpp"

VariableManager::VariableManager(int variablesCount) : _variables(variablesCount), _variablesActivity(variablesCount, 0.0), _variablesFree(variablesCount, true), _heap(variablesCount) {
	_variablesCount = variablesCount;
	_freeVariablesCount = _variablesCount;

	//for (int i = 0; i<variablesCount; i++) {
	//	_variables[i].SetFree(true);
	//}
	//_freeVariablesCount = _variablesCount;
}

bool VariableManager::AllVariablesAssigned() {
	return (_freeVariablesCount == 0);
}

int VariableManager::GetAssignedVariablesCount() {
	return _variablesCount - _freeVariablesCount;
}

void VariableManager::SetVariableFree(int variableNo) {
	_variablesFree[variableNo] = true;
	//_variables[variableNo].SetFree(true);
	_heap.InsertVariable(variableNo, _variablesActivity);
	_freeVariablesCount++;
}

void VariableManager::SetVariableAssigned(int variableNo) {
	_variablesFree[variableNo] = false;
	//_variables[variableNo].SetFree(false);
	//_heap.RemoveVariable(variableNo, _variablesActivity);
	_freeVariablesCount--;
}

bool VariableManager::VSIDSBumpConflictVariablesActivity(SharedClauseReference clause, double val, double activity_limit, double activity_rescale_factor) {
	bool rescale = false;
	
	vector<Literal>& literals = clause->GetLiterals();
	for (unsigned i = 0; i<literals.size(); i++) {
		int variableNo = literals[i].GetVariableNo();
		this->BumpVariableActivity(variableNo, val);
		if (_variablesActivity[variableNo] > activity_limit)
			rescale = true;
	}
	if (!rescale)
		return false;

	for (int i = 0; i < _variablesCount; i++) {
		_variablesActivity[i] *= activity_rescale_factor; // heap order unchanged
	}
	return true;
}

bool VariableManager::WeightedVSIDSBumpConflictVariablesActivity(SharedClauseReference clause, int conflictNo, double g, double activity_limit, double activity_rescale_factor) {
	vector<Literal>& literals = clause->GetLiterals();
	

	bool rescale = false;

	double current_g = std::pow(g, conflictNo);
	
	for (unsigned i = 0; i<literals.size(); i++) {
		int variableNo = literals[i].GetVariableNo();
		double this_weight = 0.0;
		if (i < literals.size() - 1)
			this_weight = (1e4 - i) / ((double)1e4);
		else
			this_weight = 0.5;
		this_weight *= current_g;

		this->BumpVariableActivity(variableNo, this_weight);
		if (_variablesActivity[variableNo] > activity_limit)
			rescale = true;
	}
	
	//cout << "WVSIDS conf "<< conflictNo << " weight " << current_g << " rescale " << rescale << endl;

	if (!rescale)
		return false;

	for (int i = 0; i < _variablesCount; i++) {
		_variablesActivity[i] *= activity_rescale_factor; // heap order unchanged
	}
	return true;
}

void VariableManager::BumpVariableActivity(int variableNo, double val) {
	_variablesActivity[variableNo] += val;
	_heap.IncreaseKey(variableNo, _variablesActivity);
}

void VariableManager::SetVariablePolarity(int variableNo, unsigned polarity) {
	_variables[variableNo].SetPolarity(polarity);
}

unsigned VariableManager::GetVariablePolarity(int variableNo) {
	return _variables[variableNo].GetPolarity();
}

void VariableManager::SetVariableDecisionLevel(int variableNo, int decisionLevel) {
	_variables[variableNo].SetDecisionLevel(decisionLevel);
}

int VariableManager::GetVariableDecisionLevel(int variableNo) {
	return _variables[variableNo].GetDecisionLevel();
}

void VariableManager::SetVariableReason(int variableNo, SharedClauseReference reason) {
	_variables[variableNo].SetReason(reason);
}

SharedClauseReference VariableManager::GetVariableReason(int variableNo) {
	return _variables[variableNo].GetReason();
}

unsigned int VariableManager::GetVariableDecisionLevelHash(int variableNo) {
	int variableLevel = _variables[variableNo].GetDecisionLevel();
	return 1 << (variableLevel & 31);
}

int VariableManager::GetNextVariableToAssign() {
	return _heap.GetMin(_variablesFree, _variablesActivity);
}

void VariableManager::RemoveAssignment(int variableNo) {
	this->SetVariableFree(variableNo);
	this->SetVariableReason(variableNo, nullptr);
	this->SetVariableDecisionLevel(variableNo, -1);
}

void VariableManager::UpdateClauseVariablesOccurrences(SharedClauseReference clause, int value) {
	vector<Literal>& literals = clause->GetLiterals();
	for (unsigned i = 0; i < literals.size(); i++) {
		int variableNo = literals[i].GetVariableNo();
		if (literals[i].GetSign() == LiteralSign_Positive)
			_variables[variableNo].UpdatePositiveOccurences(value);
		if (literals[i].GetSign() == LiteralSign_Negative)
			_variables[variableNo].UpdateNegativeOccurences(value);
	}
}

void VariableManager::UpdateVariableOccurrences(int variableNo, bool positive, int value) {
	if (positive)
		_variables[variableNo].UpdatePositiveOccurences(value);
	else
		_variables[variableNo].UpdateNegativeOccurences(value);
	
}

int VariableManager::GetPositiveOccurencesCount(int variableNo) {
	return _variables[variableNo].GetPositiveOccurences();
}

int VariableManager::GetNegativeOccurencesCount(int variableNo) {
	return _variables[variableNo].GetNegativeOccurences();
}
