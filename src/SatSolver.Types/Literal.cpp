

#include "Literal.hpp"


Literal::Literal() : _sign(LiteralSign_Unset), _variableNo(0) {}

Literal::Literal(unsigned variableNo, unsigned sign) : _sign(sign), _variableNo(variableNo) {
	unsigned uniqueindex = (variableNo << 1) + sign - 1;
	this->_uniqueIndex = uniqueindex;
}

Literal Literal::Negate() const {
	//unsigned sign = LiteralSign_Positive;
	//if (this->_sign == LiteralSign_Positive)
	//	sign = LiteralSign_Negative;
	assert (this->_sign != LiteralSign_Unset);

	return Literal(this->_variableNo, 3 - this->_sign);
}

LogicalValue Literal::GetValue(LogicalValue modelValue) const {
	if (modelValue == LogicalValue::None)
		return modelValue;

	if (this->_sign == LiteralSign_Positive)
		return modelValue;

	if (modelValue == LogicalValue::True)
		return LogicalValue::False;
	else
		return LogicalValue::True;
}

unsigned Literal::GetUniqueIndex() const {
	//assert (this->_sign != LiteralSign_Unset);
	//unsigned uniqueindex = (_variableNo << 1) + this->_sign - 1;
	return _uniqueIndex;
}

unsigned Literal::GetVariableNo() const {
	return _variableNo;
}

unsigned Literal::GetSign() const {
	return _sign;
}

bool Literal::operator<(const Literal& other) const {
	return (this->GetUniqueIndex() < other.GetUniqueIndex());
}

bool Literal::operator==(const Literal& other) const {
	return (this->_uniqueIndex == other.GetUniqueIndex());
}
