
#include "Literal.hpp"
#include "Clause.hpp"

class Variable {
  private:
	
	//bool _isFree;
	unsigned _polarity;
	int _decisionLevel;
	int _positiveOccurences;
	int _negativeOccurences;
	//double _activity;
	SharedClauseReference _reason;
	
  public:
	Variable();
	
	// double GetActivity();
	
	// void BumpActivity(double val);
		
	// void RescaleActivity(double factor);
	
	//void SetFree(bool free);
	
	//bool IsFree();
	
	void SetPolarity(unsigned polarity);
	
	unsigned GetPolarity();
	
	void SetDecisionLevel(int decisionLevel);
	
	int GetDecisionLevel();
	
	void SetReason(SharedClauseReference reason);
	
	SharedClauseReference GetReason();

	void UpdatePositiveOccurences(int value);

	void UpdateNegativeOccurences(int value);

	int GetPositiveOccurences();

	int GetNegativeOccurences();
};
