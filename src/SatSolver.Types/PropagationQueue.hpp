#ifndef PROPAGATIONQUEUE_H
#define PROPAGATIONQUEUE_H

#include "Literal.hpp"
#include "Clause.hpp"

class PropagationQueue {
  private:
	queue<Literal> _propagationQueue;
	
  public:
	PropagationQueue();
	
	bool Any();

	unsigned int Size();
	
	Literal RetrieveFirst();
	
	void PushLiteral(Literal literal);
	
	void Erase();
	
};

#endif
