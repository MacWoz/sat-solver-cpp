
#include "WatchedLiterals.hpp"


WatchedLiterals::WatchedLiterals(int variablesCount) : _watchedList(2*variablesCount) {
	for (int i=0;i<2*variablesCount;i++)
		_watchedList[i].reserve(16);
}

vector<SharedClauseReference>& WatchedLiterals::GetWatchingClausesForLiteral(int literalIndex) {
	return _watchedList[literalIndex];
}

unsigned int WatchedLiterals::GetWatchingClausesCountForLiteral(int literalIndex) {
	return _watchedList[literalIndex].size();
}

void WatchedLiterals::AddClauseForLiteral(SharedClauseReference clause, int literalIndex) {
	_watchedList[literalIndex].push_back(clause);
}

//void WatchedLiterals::ClearWatchingListForLiteral(int literalIndex) {
	//_watchedList[literalIndex].clear();
//}

//void WatchedLiterals::RestoreRemovedClauses(vector<SharedClauseReference>& clauses, int literalIndex) {
	//for (unsigned i=0;i<clauses.size();i++) {
		//if (!clauses[i]->IsRemoved())
			//_watchedList[literalIndex].push_back(clauses[i]);
	//}
//}
