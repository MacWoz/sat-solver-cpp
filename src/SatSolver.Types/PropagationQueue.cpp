#include "PropagationQueue.hpp"

PropagationQueue::PropagationQueue() {}

bool PropagationQueue::Any() {
	return !_propagationQueue.empty();
}

unsigned PropagationQueue::Size() {
	return _propagationQueue.size();
}

Literal PropagationQueue::RetrieveFirst() {
	Literal literal = _propagationQueue.front();
	_propagationQueue.pop();
	return literal;
}

void PropagationQueue::PushLiteral(Literal literal) {
	_propagationQueue.push(literal);	
}

void PropagationQueue::Erase() {
	//while (!_propagationQueue.empty())
	//	_propagationQueue.pop();

	queue<Literal> emptyQ;
	swap(emptyQ, _propagationQueue);
}
