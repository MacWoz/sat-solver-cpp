
#include "Clause.hpp"

//Clause::Clause() : _literals(vector<Literal>()), _isLearnt(true) {}

Clause::Clause(vector<Literal>& literals, bool isLearnt, int id, int width) {
	_header._isLearnt = isLearnt;
	_header._removed = 0;
	_header._frozen = 0;
	_header._size = literals.size();
	_header._lbd = 0;
	_header._id = id;
	_header._width = width;
	
	_activity = 0.0;
	_literals = literals;
	
}

SharedClauseReference Clause::MakeSharedReference(vector<Literal>& literals, bool fromConflict, int id, int width) {
	return make_shared<Clause>(literals, fromConflict, id, width);
}

const shared_ptr<Clause> Clause::EmptyClause = nullptr;

//Clause Clause::Empty() {
//	return Clause();
//}

vector<Literal>& Clause::GetLiterals() {
	return _literals;
}

unsigned int Clause::LiteralsCount() {
	return _header._size;
}

bool Clause::IsLearnt() {
	return _header._isLearnt;
}

unsigned int Clause::Identity() {
	return _header._id;
}

bool Clause::IsEmpty() {
	return (_header._size == 0);
}

bool Clause::IsRemoved() {
	return _header._removed;
}

void Clause::SetRemoved() {
	_header._removed = true;
}

unsigned int Clause::GetLBD() {
	return _header._lbd;
}

unsigned int Clause::GetWidth() {
	return _header._width;
}

void Clause::SetLBD(unsigned int value) {
	_header._lbd = value;
}

void Clause::SetBlocker(Literal& lit) {
	_blocker = lit;
}

Literal& Clause::GetBlocker() {
	return _blocker;
}

bool Clause::IsFrozen() {
	return _header._frozen;
}

void Clause::SetFrozen(bool frozen) {
	_header._frozen = frozen;
}

void Clause::BumpActivity(double value) {
	_activity += value;
}

void Clause::RescaleActivity(double factor) {
	_activity *= factor;
}

double Clause::GetActivity() {
	return _activity;
}

bool Clause::IsSatisfied(vector<LogicalValue>& model) {
	for (unsigned i = 0; i<_literals.size(); i++) {
		int variableNo = _literals[i].GetVariableNo();
		LogicalValue val = _literals[i].GetValue(model[variableNo]);
		if (val == LogicalValue::True)
			return true;
	}

	return false;
}

bool Clause::Simplify(vector<LogicalValue>& model, vector<Literal>& removedLiterals) {
	int pos = 0;
	
	bool todelete = false;

	for (unsigned i = 0; i<_literals.size(); i++) {
		int variableNo = _literals[i].GetVariableNo();
		LogicalValue val = _literals[i].GetValue(model[variableNo]);

		if (val == LogicalValue::True) // may be deleted - is currently satisfied
			todelete = true;
		if (val == LogicalValue::False) { // remove literal as it is always false
			removedLiterals.push_back(_literals[i]);
			continue;
		}

		_literals[pos] = _literals[i];
		pos++;
	}
	_literals.resize(pos);
	_header._size = pos;

	if (pos > 0)
		_blocker = _literals[0];

	return todelete;
}

//Check if this subsumes other = this is subset of other
bool Clause::Subsumes(SharedClauseReference other) {
	if (this->Identity() == other->Identity())
		return false;
	if (this->LiteralsCount() > other->LiteralsCount())
		return false;
	if (this->IsRemoved() || other->IsRemoved())
		return false;

	vector<Literal>& c1Literals = this->GetLiterals();
	vector<Literal>& c2Literals = other->GetLiterals();

	for (unsigned i = 0; i < c1Literals.size(); i++) {
		bool literal_found = false;
		Literal l1 = c1Literals[i];
		for (unsigned j = 0; j < c2Literals.size(); j++) {
			Literal l2 = c2Literals[j];
			if (l1 == l2) {
				literal_found = true;
				break;
			}
		}
		if (!literal_found)
			return false;
	}
	return true;
}

// Check if this clause can be strenghten (by self-subsumption) - if other is almost subset of this - one literal is negated
// other: (~x or a) this: (x or a or b) => this: (a or b)
// Effective preprocessing in SAT
int Clause::CanBeStrenghten(SharedClauseReference other) {
	if (this->LiteralsCount() <= 2) // don't strenghten binary clauses; this should not happen anyway
		return -1;
	if (this->Identity() == other->Identity())
		return -1;
	if (other->LiteralsCount() >= this->LiteralsCount())
		return -1;
	if (this->IsRemoved() || other->IsRemoved())
		return -1;
	bool negatedLiteralFound = false;
	int resultIndex = -1;

	vector<Literal>& c1Literals = other->GetLiterals();
	vector<Literal>& c2Literals = this->GetLiterals();

	for (unsigned i = 0; i < c1Literals.size(); i++) {
		bool literal_found = false;
		Literal l1 = c1Literals[i];
		for (unsigned j = 0; j < c2Literals.size(); j++) {
			Literal l2 = c2Literals[j];
			Literal l2n = l2.Negate();

			if (l1 == l2) {
				literal_found = true;
				break;
			}
			if (l1 == l2n) {
				if (negatedLiteralFound) // more than one negated found
					return -1;
				negatedLiteralFound = true;
				literal_found = true; // for next if to work
				resultIndex = j;
				break;
			}
		}
		if (!literal_found)
			return -1;
	}
	if ((resultIndex == 0) || (resultIndex == 1)) // if literal is watched - do not touch it - may cause errors
		resultIndex = -1;

	return resultIndex; // -1 if cannot be strenghten
}

// remove unnecesary literal (subsumption)
void Clause::Strenghten(int literalIndex) {
	std::swap(_literals[literalIndex], _literals[_literals.size() - 1]);
	_literals.pop_back();
	_header._size -= 1;
	_blocker = _literals[0];
}

void Clause::Print() {
	for (unsigned i = 0; i<_literals.size(); i++) {
		if (_literals[i].GetSign() == LiteralSign_Negative)
			std::cout << "NOT ";
		std::cout << _literals[i].GetVariableNo() << " ; ";
	}
	std::cout << endl;
}
