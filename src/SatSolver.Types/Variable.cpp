#include "Variable.hpp"

Variable::Variable() : _polarity(LiteralSign_Unset), _decisionLevel(-1), _positiveOccurences(0), _negativeOccurences(0), _reason(nullptr) {}

// double Variable::GetActivity() {
// 	return _activity;
// }

// void Variable::BumpActivity(double val) {
// 	this->_activity += val;
// }

// void Variable::RescaleActivity(double factor) {
// 	this->_activity *= factor;
// }

//void Variable::SetFree(bool free) {
	//this->_isFree = free;
//}

//bool Variable::IsFree() {
	//return this->_isFree;
//}

void Variable::SetPolarity(unsigned polarity) {
	_polarity = polarity;
}

unsigned Variable::GetPolarity() {
	return _polarity;
}

void Variable::SetDecisionLevel(int decisionLevel) {
	_decisionLevel = decisionLevel;
} 

int Variable::GetDecisionLevel() {
	return _decisionLevel;
}

void Variable::SetReason(SharedClauseReference reason) {
	_reason = reason;
}

SharedClauseReference Variable::GetReason() {
	return _reason;
}


void Variable::UpdatePositiveOccurences(int value) {
	_positiveOccurences += value;
}

void Variable::UpdateNegativeOccurences(int value) {
	_negativeOccurences += value;
}

int Variable::GetPositiveOccurences() {
	return _positiveOccurences;
}

int Variable::GetNegativeOccurences() {
	return _negativeOccurences;
}
