
#include "BoundedArrayQueue.hpp"

BoundedArrayQueue::BoundedArrayQueue(int capacity) : _capacity(capacity), _size(0), _nextIndex(0), _sum(0), _data(capacity, 0) {}
	
void BoundedArrayQueue::Insert(int value) {
	if (this->IsFull()) 
		_sum -= ((long long)_data[_nextIndex]); // remove oldest element from sum - will be replaced in queue
	else
		_size++;
	
	_data[_nextIndex] = value;
	_sum += ((long long)_data[_nextIndex]); // add new element to sum
	
	_nextIndex++;
	if (_nextIndex == _capacity)
		_nextIndex = 0;
}
	
double BoundedArrayQueue::Average() {
	return ((double)_sum) / _size;
}
	
bool BoundedArrayQueue::IsFull() {
	return (_size == _capacity);
}
	
void BoundedArrayQueue::Clear() {
	_size = 0;
	_nextIndex = 0;
	_sum = 0;
	
	
}
