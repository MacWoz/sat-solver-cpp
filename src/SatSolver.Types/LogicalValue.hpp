#ifndef LOGICALVALUE_H
#define LOGICALVALUE_H

enum LogicalValue {
	None = 0,
	True = 1,
	False = 2
};

#endif

