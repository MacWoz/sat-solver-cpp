#ifndef TRAIL_H
#define TRAIL_H

#include "Literal.hpp"

class Trail {
  private:
	vector<Literal> _stack;
	vector<int> _decisionLevels;
  
  public:
	Trail(int variablescount);
	
	int GetCurrentDecisionLevel();
	
	int GetSize();
	
	Literal At(int index);
	
	void PushLiteral(Literal literal);
	
	void NewDecisionLevel();
	
	Literal RetrieveLast();
};


#endif
