#include "Trail.hpp"

Trail::Trail(int variablescount) {
	_stack.reserve(variablescount);
	_decisionLevels.reserve(variablescount / 16);
}

int Trail::GetCurrentDecisionLevel() {
	return _decisionLevels.size();
}

int Trail::GetSize() {
	return _stack.size();
}

void Trail::PushLiteral(Literal literal) {
	_stack.push_back(literal);
}

void Trail::NewDecisionLevel() {
	_decisionLevels.push_back(_stack.size());
}

Literal Trail::At(int index) {
	return _stack[index];
}

Literal Trail::RetrieveLast() {
	Literal r = _stack.back();
	_stack.pop_back();
	
	if (!_decisionLevels.empty()) {
		if (this->GetSize() == _decisionLevels.back()) {
			//cancel decision level
			_decisionLevels.pop_back();
		}
	}
	
	return r;
}
