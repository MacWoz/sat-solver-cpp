#ifndef WATCHEDLITERALS_H
#define WATCHEDLITERALS_H

#include "Literal.hpp"
#include "Clause.hpp"

class WatchedLiterals {
	
  public:
	WatchedLiterals(int variablesCount);
	
	vector<SharedClauseReference>& GetWatchingClausesForLiteral(int literalIndex);

	unsigned int GetWatchingClausesCountForLiteral(int literalIndex);
	
	void AddClauseForLiteral(SharedClauseReference clause, int literalIndex);
	
	//void ClearWatchingListForLiteral(int literalIndex);
	
	//void RestoreRemovedClauses(vector<SharedClauseReference>& clauses, int literalIndex);
	
  private:
	vector<vector<SharedClauseReference>> _watchedList;

};

#endif // WATCHEDLITERALS_H
