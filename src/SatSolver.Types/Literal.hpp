#ifndef LITERAL_H
#define LITERAL_H

#include <iostream>
#include <vector>
#include <set>
#include <memory>
#include <queue>
#include <fstream>
#include <string>
#include <algorithm>
#include <cassert>

using namespace std;

#include "LiteralSign.hpp"
#include "LogicalValue.hpp"

class Literal {
  private:
	unsigned _sign			: 2;
	unsigned _variableNo  	: 30;
	unsigned _uniqueIndex	: 32;
	
  public:
	
	// only for vector resizing - should not be used
	Literal();
  
	Literal(unsigned variableNo, unsigned sign);
	
	Literal Negate() const;
	
	LogicalValue GetValue(LogicalValue modelValue) const;
	
	unsigned GetUniqueIndex() const;
	
	unsigned GetVariableNo() const;
	
	unsigned GetSign() const;
	
	//only for sorting in AddClause
	bool operator<(const Literal& other) const;
	
	bool operator==(const Literal& other) const;
};

#endif //LITERAL_H
