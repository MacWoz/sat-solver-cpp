#ifndef SOLVERSTATE_H
#define SOLVERSTATE_H

enum SolverState {
	InternalError = -1,
	Unknown = 0,
	InProgress = 1,
	Restart = 2,
	Satisfiable = 3,
	Unsatisfiable = 4
};

#endif
