#ifndef VARIABLEORDERHEAP_H
#define VARIABLEORDERHEAP_H

#include "Literal.hpp"

class VariableOrderHeap {

private:
    int _size;
    int _capacity;
	
    vector<int> _heap;
    vector<int> _variablesIndices;

    bool VariableExists(int variableNo);

    bool CompareActivities(int index1, int index2, vector<double>& variablesActivity);

    int GetMaxChildIndex(int parentIndex, vector<double>& variablesActivity);

    int GetParentIndex(int heapIndex);

    int GetLeftChildIndex(int heapIndex);

    int GetRightChildIndex(int heapIndex);

    bool HasChild(int heapIndex);

    int PropagateUp(int heapIndex, vector<double>& variablesActivity);

    void PropagateDown(int heapIndex, vector<double>& variablesActivity);
    
	void RemoveVariable(int variableNo, vector<double>& variablesActivity);

public:
    VariableOrderHeap(int capacity);

    void IncreaseKey(int variableNo, vector<double>& variablesActivity);

    void InsertVariable(int variableNo, vector<double>& variablesActivity);

    int GetMin(vector<bool>& variablesFree, vector<double>& variablesActivity);




};



#endif // VARIABLEORDERHEAP_H
