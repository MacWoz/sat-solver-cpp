#ifndef LITERALSIGN_H
#define LITERALSIGN_H

//enum LiteralSign {
	//Unset = -1, // only for polarity
	//Positive = 0,
	//Negative = 1
//};

const unsigned int LiteralSign_Unset = 0;
const unsigned int LiteralSign_Positive = 1;
const unsigned int LiteralSign_Negative = 2;

#endif
