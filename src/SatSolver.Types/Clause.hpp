#ifndef CLAUSE_H
#define CLAUSE_H

#include "Literal.hpp"
#include "LogicalValue.hpp"
#include "ClauseHeader.hpp"

class Clause;

typedef shared_ptr<Clause> SharedClauseReference;

class Clause {
public:
	//Clause();

	Clause(vector<Literal>& literals, bool isLearnt, int id, int width);

	static SharedClauseReference MakeSharedReference(vector<Literal>& literals, bool fromConflict, int id, int width);

	const static shared_ptr<Clause> EmptyClause;

	vector<Literal>& GetLiterals();

	unsigned int LiteralsCount();

	unsigned int Identity();

	bool IsLearnt();

	bool IsEmpty();

	bool IsRemoved();

	void SetRemoved();

	bool IsSatisfied(vector<LogicalValue>& model);

	bool Simplify(vector<LogicalValue>& model, vector<Literal>& removedLiterals);

	void BumpActivity(double value);

	void RescaleActivity(double factor);

	double GetActivity();

	unsigned int GetLBD();
	
	unsigned int GetWidth();

	void SetLBD(unsigned int value);

	void SetBlocker(Literal& lit);

	Literal& GetBlocker();

	bool IsFrozen();

	void SetFrozen(bool frozen);

	bool Subsumes(SharedClauseReference other);

	int CanBeStrenghten(SharedClauseReference other);

	void Strenghten(int literalIndex);

	void Print();

private:
	ClauseHeader _header;
	double _activity;
	Literal _blocker;
	vector<Literal> _literals;
};

#endif
