#ifndef CLAUSEHEADER_H
#define CLAUSEHEADER_H

struct ClauseHeader {
	unsigned _isLearnt  : 1;
	unsigned _removed   : 1;
	unsigned _frozen    : 1;    
	unsigned _size      : 29;
	unsigned _lbd 		: 32;
	unsigned _id		: 32;
	unsigned _width		: 32;	
};


#endif
