#include "VariableOrderHeap.hpp"

VariableOrderHeap::VariableOrderHeap(int capacity) : _size(0), _capacity(capacity), 
                                                                        _heap(capacity, -1), _variablesIndices(capacity, -1)
{
    for (int i=0;i<capacity;i++)
        _heap[i] = _variablesIndices[i] = i;

    _size = _capacity;
}

int VariableOrderHeap::GetParentIndex(int heapIndex) {
    return (heapIndex - 1) / 2;
}

int VariableOrderHeap::GetLeftChildIndex(int heapIndex) {
    return (2 * heapIndex + 1);
}

int VariableOrderHeap::GetRightChildIndex(int heapIndex) {
    return (2 * heapIndex + 2);
}

bool VariableOrderHeap::HasChild(int heapIndex) {
    int leftIndex = GetLeftChildIndex(heapIndex);
    return (leftIndex < _size);
}

int VariableOrderHeap::GetMin(vector<bool>& variablesFree, vector<double>& variablesActivity) {
	while (true) {
		int variableNo = _heap[0];
		if (variablesFree[variableNo])
			break;
		else 
			RemoveVariable(variableNo, variablesActivity);
	}
	
	
	return _heap[0];

    /*int bestvar = _heap[0];
    _heap[0] = _heap[_size - 1];
    _variablesIndices[_heap[_size - 1]] = 0;
    _variablesIndices[bestvar] = -1;
    _size -= 1;
    cout << "heap size ext " << _size << " " << _heap.size() << endl;

    if (_size > 1)
        PropagateDown(0);

    return bestvar;*/
}

void VariableOrderHeap::RemoveVariable(int variableNo, vector<double>& variablesActivity) {
	if (!VariableExists(variableNo))
		return;
	
	int heapIndex = _variablesIndices[variableNo];
	_heap[heapIndex] = _heap[_size - 1];
	_variablesIndices[_heap[_size - 1]] = heapIndex;
	_size -= 1;
	_variablesIndices[variableNo] = -1;

	if (heapIndex < _size)
	{
		heapIndex = PropagateUp(heapIndex, variablesActivity);
		PropagateDown(heapIndex, variablesActivity);
	}
}

bool VariableOrderHeap::CompareActivities(int index1, int index2, vector<double>& variablesActivity) {
    return (variablesActivity[index1] > variablesActivity[index2]);
}

void VariableOrderHeap::IncreaseKey(int variableNo, vector<double>& variablesActivity) {
    if (!VariableExists(variableNo))
        return;
    
    PropagateUp(_variablesIndices[variableNo], variablesActivity);
}

void VariableOrderHeap::InsertVariable(int variableNo, vector<double>& variablesActivity) {
	if (VariableExists(variableNo))
		return;
		
    _size++;

    //cout << "heap size ins" << _size << " " << _heap.size() << endl;
    _variablesIndices[variableNo] = _size - 1;
    _heap[_size - 1] = variableNo;

    PropagateUp(_size - 1, variablesActivity);
}

bool VariableOrderHeap::VariableExists(int variableNo) {
    return (_variablesIndices[variableNo] > -1);
}

int VariableOrderHeap::PropagateUp(int heapIndex, vector<double>& variablesActivity) {
	if (_size == 0)
		return -1;


    int currentHeapIndex = heapIndex;
    int propValue  = _heap[currentHeapIndex];
    int parentIndex  = GetParentIndex(currentHeapIndex);
    
    while (currentHeapIndex > 0 && CompareActivities(propValue, _heap[parentIndex], variablesActivity)){
        _heap[currentHeapIndex] = _heap[parentIndex];
        _variablesIndices[_heap[parentIndex]] = currentHeapIndex;
        currentHeapIndex =  parentIndex;
        parentIndex = GetParentIndex(parentIndex);
    }
    _heap[currentHeapIndex] = propValue;
    _variablesIndices[propValue] = currentHeapIndex;

	return currentHeapIndex;
}

int VariableOrderHeap::GetMaxChildIndex(int parentIndex, vector<double>& variablesActivity) {
    int leftIndex = GetLeftChildIndex(parentIndex);
    int rightIndex = GetRightChildIndex(parentIndex);

    int maxIndex = leftIndex;
    if (rightIndex < _size)
        maxIndex = rightIndex;
    
    if (CompareActivities(_heap[leftIndex], _heap[maxIndex], variablesActivity))
        maxIndex = leftIndex;

    return maxIndex;
}

void VariableOrderHeap::PropagateDown(int heapIndex, vector<double>& variablesActivity) {
	if (_size == 0)
		return;

    int currentHeapIndex = heapIndex;
    int propValue  = _heap[currentHeapIndex];

    while (HasChild(currentHeapIndex)) {
        int childIndex = GetMaxChildIndex(currentHeapIndex, variablesActivity);
        if (CompareActivities(propValue, _heap[childIndex], variablesActivity)) // max smaller than parent
            break;

        _heap[currentHeapIndex] = _heap[childIndex];
        _variablesIndices[_heap[childIndex]] = currentHeapIndex;
        currentHeapIndex = childIndex;
    }
    _heap[currentHeapIndex] = propValue;
    _variablesIndices[propValue] = currentHeapIndex;
}
