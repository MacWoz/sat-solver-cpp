
#include "./SatSolver.Core/Solver.hpp"
#include "./SatSolver.Parser/Parser.hpp"
#include <chrono>
#include <cassert>

void RunTest(string folderPath, string testPrefix, int testNo);

void RunTestFromStandardInput();

//int main(/*int argc, char* argv[]*/) {

	//std::ios_base::sync_with_stdio(false);

	///*if (argc <= 1) {
	//std::cout << "ERROR: No input file path given - exiting" << std::endl;
	//return 1;
	//}*/

	//string folderPath = "../benchmarks/SATLIB/uf250-1065/";

	//int testNo = 1;
	//int testsCount = 100;
	//string testPrefix = "uf250-0";
	//for (; testNo <= testsCount; testNo++) {
		//RunTest(folderPath, testPrefix, testNo);
	//}



	////getchar();

	//return 0;
//}

//stdin
 int main() {

 	std::ios_base::sync_with_stdio(false);

 	int testsCount = 1;
 	//cin >> testsCount;
	
 	while (testsCount--) {
 		RunTestFromStandardInput();	
 	}



 	//getchar();

 	return 0;
 }

void RunTest(string folderPath, string testPrefix, int testNo) {
	cout << "Test number " << testNo << " start" << endl;
	std::chrono::time_point<std::chrono::high_resolution_clock> start = std::chrono::high_resolution_clock::now();

	string testFilePath = folderPath + testPrefix + std::to_string(testNo) + ".cnf";
	CNFParser parser = CNFParser();
	ParseResult parseResult = parser.ParseCNFFormula(testFilePath.c_str());

	if (parseResult.Failed()) {
		std::cout << "ERROR: " << parseResult.GetErrorMessage() << " - exiting" << std::endl;
		return;
	}

	std::chrono::time_point<std::chrono::high_resolution_clock> afterInputParsing = std::chrono::high_resolution_clock::now();
	std::cout << "Parsing finished - time used: " << std::chrono::duration_cast<std::chrono::milliseconds>(afterInputParsing - start).count() << "ms" << std::endl;

	Solver solver = Solver(parseResult.GetVariablesCount(), parseResult.Clauses.size());
	for (unsigned i = 0; i<parseResult.Clauses.size(); i++) {
		solver.AddClause(parseResult.Clauses[i], false, 0);
	}

	solver.SolveSATInstance();

	std::chrono::time_point<std::chrono::high_resolution_clock> onInstanceSolved = std::chrono::high_resolution_clock::now();
	std::cout << "Solving finished - time used: " << std::chrono::duration_cast<std::chrono::milliseconds>(onInstanceSolved - afterInputParsing).count() << "ms" << std::endl;
	std::cout << "Total time used: " << std::chrono::duration_cast<std::chrono::milliseconds>(onInstanceSolved - start).count() << "ms" << std::endl;

	//assert(solver.GetState() == SolverState::Unsatisfiable);
	//if (solver.GetState() != SolverState::Satisfiable) {
	//	exit(3);
	//}

	solver.CheckModelCorrectness();
	solver.PrintResult();
	cout << "Test number " << testNo << " end" << endl;
	cout << endl;
}

void RunTestFromStandardInput() {
	
	cout << "c Test start input" << endl;
	std::chrono::time_point<std::chrono::high_resolution_clock> start = std::chrono::high_resolution_clock::now();
	
	CNFParser parser = CNFParser();
	ParseResult parseResult = parser.ParseCNFFormulaFromStandardInput();

	if (parseResult.Failed()) {
		std::cout << "c ERROR: " << parseResult.GetErrorMessage() << " - exiting" << std::endl;
		return;
	}
	
	std::chrono::time_point<std::chrono::high_resolution_clock> afterInputParsing = std::chrono::high_resolution_clock::now();
	std::cout << "c Parsing finished - time used: " << std::chrono::duration_cast<std::chrono::milliseconds>(afterInputParsing - start).count() << "ms" << std::endl;
	
	Solver solver = Solver(parseResult.GetVariablesCount(), parseResult.Clauses.size());
	for (unsigned i = 0; i<parseResult.Clauses.size(); i++) {
		solver.AddClause(parseResult.Clauses[i], false, 0);
	}

	solver.SolveSATInstance();
	
	std::chrono::time_point<std::chrono::high_resolution_clock> onInstanceSolved = std::chrono::high_resolution_clock::now();
	std::cout << "c Solving finished - time used: " << std::chrono::duration_cast<std::chrono::milliseconds>(onInstanceSolved - afterInputParsing).count() << "ms" << std::endl;
	std::cout << "c Total time used: " << std::chrono::duration_cast<std::chrono::milliseconds>(onInstanceSolved - start).count() << "ms" << std::endl;

	
	solver.CheckModelCorrectness();
	solver.PrintResult();
	
	cout << "c Test end" << endl;
}
