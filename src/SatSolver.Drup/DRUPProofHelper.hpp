#ifndef DRUPROOFHELPER_H
#define DRUPROOFHELPER_H

#include "../SatSolver.Types/Literal.hpp"

#include <sstream>
#include <fstream>
#include <string>

class DRUPProofHelper {
public:
	DRUPProofHelper(std::string proofFilePath);
	
	void AddClause(std::vector<Literal>& literals);
	
	void RemoveClause(std::vector<Literal>& literals);
	
	void CloseProof();
	
private:
	//std::fstream _outputfile;

};


#endif
