# Coffee SAT solver
Implementacja współczesnego SAT solvera w ramach pracy magisterskiej na kierunku Informatyka Analityczna w Zespole Katedr i Zakładów Informatyki Matematycznej na Wydziale Matematyki i Informatyki Uniwersytetu Jagiellońskiego.

## Kompilacja i uruchomienie solvera
Do poprawnej kompilacji solvera wymagany jest program ````cmake```` w wersji co najmniej ````2.8.9```` oraz kompilator ````g++```` obsługujący standard ````c++11````.

W celu uruchomienia Coffee należy:

* Sklonować repozytorium z kodem źródłowym: ````git clone https://bitbucket.org/MacWoz/sat-solver-cpp.git````
* Przejść do folderu ````sat-solver-cpp````  
* Wykonać polecenie ````cmake .````
* Wykonać polecenie ````make````
* Jeśli kompilacja się powiodła, powstał plik wykonywalny o nazwie ````solver````
* Można uruchomić solver na dwa sposoby: albo poleceniem ````./solver```` (wtedy benchmark do rozwiązania będzie wczytywany ze standardowego wejścia) albo ````./solver < benchmark_path```` - wtedy benchmark zostanie wczytany z pliku.